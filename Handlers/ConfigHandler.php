<?php

namespace Steady\Engine\Handlers;

use yii\web\ServerErrorHttpException;

class ConfigHandler
{
    /**
     * @name string $file
     * @name string $config
     * @return bool
     * @throws ServerErrorHttpException
     */
    public function createPersonalConfigFile(string $file, string $config): bool
    {
        if (!file_exists(STD_PATH_CONFIG_PERSONAL)) {
            throw new ServerErrorHttpException('Configuration folder does not exist');
        }
        try {
            $fp = fopen($file, "w");
            $writeResult = fwrite($fp, str_replace(' ', '', $config));
            $closeResult = fclose($fp);

            if ($writeResult && $closeResult) {
                return true;
            } else {
                throw new ServerErrorHttpException('Failed to save configuration');
            }
        } catch (\Exception $e) {
            throw new ServerErrorHttpException('Failed to create configuration');
        }
    }
}