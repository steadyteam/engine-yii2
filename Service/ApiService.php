<?php

namespace Steady\Engine\Service;

use Steady\Engine\Base\Api;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Exceptions\NotImplementedException;
use Steady\Engine\Modules\Block\Api\BlockApi;
use Steady\Engine\Modules\Layout\Api\LayoutApi;
use Steady\Engine\Modules\Page\Api\PageApi;
use Steady\Engine\Modules\Text\Api\TextApi;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Api\CatalogApi;
use Steady\Modules\Comment\Api\CommentApi;
use Steady\Modules\Email\Api\EmailApi;
use Steady\Modules\Gallery\Api\GalleryApi;
use Steady\Modules\Shop\Api\ShopApi;
use yii\base\BaseObject;

/**
 * @property LayoutApi layout
 * @property PageApi page
 * @property BlockApi block
 * @property TextApi text
 * @property CatalogApi catalog
 * @property ShopApi shop
 * @property GalleryApi gallerys
 * @property CommentApi comment
 * @property EmailApi email
 */
class ApiService extends BaseObject
{
    /**
     * @var Api[]
     */
    public $map = [];

    /**
     * @var Api[]
     */
    protected $objects = [];

    /**
     * @name string $method
     * @name array $params
     * @return Api|ApiObject
     */
    public static function __callStatic(string $method, array $params)
    {
        return SW::$app->api->getApiClass($method, $params);
    }

    /**
     * @name string $name
     * @name array $arguments
     * @return Api|ApiObject
     */
    public function __call($name, $arguments)
    {
        return $this->getApiClass($name, $arguments);
    }

    /**
     * @name string $name
     * @return Api|ApiObject
     */
    public function __get($name)
    {
        return $this->getApiClass($name, []);
    }

    /**
     * @name string $method
     * @name array $params
     * @return Api|ApiObject
     */
    public function getApiClass(string $method, array $params)
    {
        if (!isset($this->map[$method]) || !class_exists($this->map[$method])
            || !is_subclass_of($this->map[$method], Api::class)) {
            throw new NotImplementedException("API '$method' not found");
        }
        $class = $this->map[$method];

        if (!isset($this->objects[$method])) {
            $this->objects[$method] = new $class();
        }
        $object = $this->objects[$method];

        if (isset($params[0])) {
            return $object->get($params[0]);
        } else {
            return $object;
        }
    }
}