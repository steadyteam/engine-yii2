<?php

defined('STD_FILENAME_DB_PERSONAL_CONFIG') or define('STD_FILENAME_DB_PERSONAL_CONFIG', STD_PATH_CONFIG_PERSONAL . '/db.personal.ini');
defined('STD_FILENAME_ADMIN_PERSONAL_CONFIG') or define('STD_FILENAME_ADMIN_PERSONAL_CONFIG', STD_PATH_CONFIG_PERSONAL . '/admin.personal.ini');

if (file_exists(STD_FILENAME_DB_PERSONAL_CONFIG)) {
    $array = parse_ini_file(STD_FILENAME_DB_PERSONAL_CONFIG);
}

$driver = $array['driver'] ?? 'mysql';
$host = $array['host'] ?? 'localhost';
$dbname = $array['dbname'] ?? 'schema';

return [
    'class' => 'yii\db\Connection',
    'dsn' => "$driver:host=$host;dbname=$dbname",
    'username' => $array['username'] ?? 'root',
    'password' => $array['password'] ?? '',
    'charset' => $array['charset'] ?? 'utf8',
    'tablePrefix' => $array['tablePrefix'] ?? '',
    'enableSchemaCache' => $array['enableSchemaCache'] ?? true,
];