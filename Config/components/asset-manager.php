<?php

return [
    'basePath' => '@assets',
    'baseUrl' => '@url-assets',
    'forceCopy' => true,
    'bundles' => [
    ],
];