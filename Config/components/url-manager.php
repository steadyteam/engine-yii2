<?php

return [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        'debug/<controller>/<action>' => 'debug/<controller>/<action>',

        // Admin
        'cp' => 'admin',
        'admin/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => 'admin/<controller>/<action>',
        'admin/<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => 'admin/<module>/<controller>/<action>',

        // Sitemap
        ['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml'],

        // Catalog
        'catalog' => 'catalog/catalog',
        'category/<slug:[\w-]+>' => 'catalog/category',
        'brand/<slug:[\w-]+>' => 'catalog/brand',
        'goods/<slug:[\w-]+>' => 'catalog/goods',
        'goods/<slug:[\w-]+>/<offer:[\w-]+>' => 'catalog/goods',
        'tag/<slug:[\w-]+>' => 'catalog/tag',

        'favourites' => 'catalog/favourites',

        'category/<slug:[\w-]+>/<brand:[\w-]+>' => 'catalog/category',
        'brand/<slug:[\w-]+>/<category:[\w-]+>' => 'catalog/brand',

        'catalog/<slug:[\w-]+>' => 'catalog/category',
        'catalog/category/<slug:[\w-]+>' => 'catalog/category',
        'catalog/brand/<slug:[\w-]+>' => 'catalog/brand',
        'catalog/goods/<slug:[\w-]+>/<offer:[\w-]+>' => 'catalog/goods',
        'catalog/tag/<slug:[\w-]+>' => 'catalog/tag',

        // Shop
        'cart' => 'shop/cart',
        'order' => 'shop/order',

        // User
        'login' => '/user/login',
        'registration' => '/user/registration',

        'user/login' => '/user/login',
        'user/registration' => '/user/registration',

        'user/orders' => 'shop/orders',

        // Modules
        //['class' => 'Steady\Modules\Catalog\Components\CatalogUrlRule']
        ['class' => 'Steady\Engine\Modules\Page\Components\PageUrlRule'],
    ],
];