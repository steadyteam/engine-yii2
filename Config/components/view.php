<?php

use Steady\Engine\SW;

return [
    'class' => 'yii\web\View',
    'defaultExtension' => 'twig',
    'renderers' => [
        'twig' => [
            'class' => 'yii\twig\ViewRenderer',
            'cachePath' => '@runtime/Twig/cache',
            'options' => [
                'auto_reload' => true,
            ],
            'extensions' => [
                //Twig_Extension_Debug::class,
            ],
            'globals' => [
                'html' => '\yii\helpers\Html',
                'url' => '\yii\helpers\Url',
                'sw' => \Steady\Engine\SW::class,
                'api' => \Steady\Engine\Service\ApiService::class,
            ],
            'functions' => [
                'create_form' => function ($namespace) {
                    if (class_exists($namespace)) {
                        return new $namespace();
                    }
                    return null;
                },
                'active_url' => function ($url) {
                    return $url === SW::$app->request->url ? 'st_active' : '';
                },
                'img_view' => function ($url) {
                    return $url ?: SW::getAlias('@url-static/img/base/no_image.svg');
                },
                'print_r' => function ($expression) {
                    return print_r($expression);
                },
            ],
            'uses' => ['yii\bootstrap'],
        ],
    ],
];