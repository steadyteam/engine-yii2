<?php

return [
    'class' => \Steady\Engine\Service\ApiService::class,
    'map' => [
        'layout' => \Steady\Engine\Modules\Layout\Api\LayoutApi::class,
        'page' => \Steady\Engine\Modules\Page\Api\PageApi::class,
        'block' => \Steady\Engine\Modules\Block\Api\BlockApi::class,
        'text' => \Steady\Engine\Modules\Text\Api\TextApi::class,
        'catalog' => \Steady\Modules\Catalog\Api\CatalogApi::class,
        'gallery' => \Steady\Modules\Gallery\Api\GalleryApi::class,
        'shop' => \Steady\Modules\Shop\Api\ShopApi::class,
        'comment' => \Steady\Modules\Comment\Api\CommentApi::class,
        'email' => \Steady\Modules\Email\Api\EmailApi::class,
    ],
];