<?php

return [
    '@root' => STD_PATH_ROOT,
    '@admin' => STD_PATH_ADMIN,
    '@backend' => STD_PATH_BACKEND,
    '@engine' => STD_PATH_ENGINE,
    '@frontend' => STD_PATH_FRONTEND,
    '@install' => STD_PATH_INSTALL,
    '@modules' => STD_PATH_MODULES,
    '@public' => STD_PATH_PUBLIC,
    '@runtime' => STD_PATH_RUNTIME,
    '@vendor' => STD_PATH_VENDOR,

    '@config-personal' => STD_PATH_CONFIG_PERSONAL,
    '@assets' => STD_PATH_PUBLIC . '/assets',
    '@static' => STD_PATH_PUBLIC . '/static',
    '@uploads' => STD_PATH_PUBLIC . '/uploads',

    '@bower' => STD_PATH_VENDOR . '/bower-asset',
    '@npm' => STD_PATH_VENDOR . '/npm-asset',

    '@url-assets' => STD_URL_ASSETS,
    '@url-dist' => STD_URL_DIST,
    '@url-static' => STD_URL_STATIC,
    '@url-uploads' => STD_URL_UPLOADS,
];
