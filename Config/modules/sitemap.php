<?php

use himiklab\sitemap\behaviors\SitemapBehavior;
use himiklab\sitemap\Sitemap;
use Steady\Engine\Base\Query;
use Steady\Engine\Modules\Page\Models\PageModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\Url;

$page = [
    'class' => PageModel::class,
    'behaviors' => [
        'sitemap' => [
            'class' => SitemapBehavior::class,
            'scope' => function (Query $query) {
                $query->select(['slug'/*, 'lastmod'*/])
                    ->where(['hidden' => 0])
                    ->andWhere(['not in', 'slug', ['base', 'index']]);
            },
            'dataClosure' => function (PageModel $model) {
                return [
                    'loc' => Url::to($model->slug, true),
                    //'lastmod' => strtotime($model->lastmod),
                    'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                    'priority' => 0.8,
                ];
            },
        ],
    ],
];

$category = [
    'class' => CategoryModel::class,
    'behaviors' => [
        'sitemap' => [
            'class' => SitemapBehavior::class,
            'scope' => function (Query $query) {
                $query->select(['slug'])->where(['>', 'depth', '0']);
            },
            'dataClosure' => function (CategoryModel $model) {
                return [
                    'loc' => Url::to("/category/$model->slug", true),
                    'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                    'priority' => 0.8,
                ];
            },
        ],
    ],
];

$goods = [
    'class' => GoodsModel::class,
    'behaviors' => [
        'sitemap' => [
            'class' => SitemapBehavior::class,
            'scope' => function (Query $query) {
                $query->select(['slug']);
            },
            'dataClosure' => function (GoodsModel $model) {
                return [
                    'loc' => Url::to("/goods/$model->slug", true),
                    'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                    'priority' => 0.8,
                ];
            },
        ],
    ],
];

$models[] = $page;
// TODO: Сделать проверку на модули. Разработать полноценную систему управления sitemap.
//$models[] = $category;
//$models[] = $goods;

return [
    'class' => Sitemap::class,
    'models' => $models,
    'enableGzip' => true, // default is false
    'cacheExpire' => 1, // 1 second, default is 24 hours
];