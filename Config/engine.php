<?php

$aliases = require(STD_PATH_ENGINE . '/Config/common/aliases.php');

$admin = require(STD_PATH_ADMIN . '/Config/admin.php');
//$install = require(STD_PATH_INSTALL . '/Config/install.php');
$sitemap = require(STD_PATH_ENGINE . '/Config/modules/sitemap.php');

$urlManager = require(STD_PATH_ENGINE . '/Config/components/url-manager.php');
$db = require(STD_PATH_ENGINE . '/Config/components/db.php');
$view = require(STD_PATH_ENGINE . '/Config/components/view.php');
$api = require(STD_PATH_ENGINE . '/Config/components/api.php');
$assetsManager = require(STD_PATH_ENGINE . '/Config/components/asset-manager.php');

$config = [
    'timeZone' => 'Europe/Moscow',
    'basePath' => STD_PATH_BACKEND,
    'runtimePath' => STD_PATH_RUNTIME,
    'vendorPath' => STD_PATH_VENDOR,
    'aliases' => $aliases,
    'bootstrap' => ['log', 'admin'],
    'modules' => [
        'admin' => $admin,
        //'install' => $install,
        'sitemap' => $sitemap,
    ],
    'components' => [
        'urlManager' => $urlManager,
        'db' => $db,
        'view' => $view,
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'api' => $api,
        'user' => [
            'identityClass' => 'Steady\Engine\Modules\User\Models\UserModel',
            'enableAutoLogin' => true,
            'authTimeout' => 86400,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
                [
                    'class' => 'yii\log\EmailTarget',
                    'levels' => ['error', 'warning', 'info'],
                    'message' => [
                        'from' => ['debug@steady-web.com'],
                        'to' => ['bezhin.nikita1996@gmail.com'],
                        'subject' => 'Steady Web Debug',
                    ],
                ],
            ],
        ],
        /*'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],*/
        'formatter' => [
            'sizeFormatBase' => 1000,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
        ],
        'i18n' => [
            'translations' => [
                'admin' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@admin/Resources/messages',
                    'fileMap' => [
                        'admin' => 'admin.php',
                        'admin/install' => 'install.php',
                    ],
                ],
            ],
        ],
        'assetManager' => $assetsManager,
    ],
];

if (YII_ENV_DEV) {
    $dev = [
        'bootstrap' => ['gii', 'debug'],
        'components' => [
            'db' => [
                'enableSchemaCache' => false,
            ],
        ],
        'modules' => [
            'gii' => [
                'class' => 'yii\gii\Module',
                'allowedIPs' => ['*'],
            ],
            'debug' => [
                'class' => 'yii\debug\Module',
                'allowedIPs' => ['*'],
            ],
        ],
    ];
    $config = array_merge_recursive($config, $dev);
}

return $config;