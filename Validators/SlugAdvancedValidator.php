<?php

namespace Steady\Engine\Validators;

use Steady\Engine\Base\Model;
use Steady\Engine\SW;
use yii\validators\Validator;

class SlugAdvancedValidator extends Validator
{
    const SLUG_ADVANCED_PATTERN = '/^[0-9a-z-_\/]{0,512}$/';

    /**
     * @var string
     */
    public $pattern = self::SLUG_ADVANCED_PATTERN;

    /**
     * @name Model $model
     * @name string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match($this->pattern, $model->$attribute)) {
            $this->addError($model, $attribute, SW::t('admin', SlugValidator::SLUG_MESSAGE_RULES));
        }
    }
}