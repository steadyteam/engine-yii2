<?php

namespace Steady\Engine\Validators;

use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\validators\Validator;

class ReCaptchaValidator extends Validator
{
    const SITE_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';

    const CAPTCHA_RESPONSE_FIELD = 'g-recaptcha-response';

    /**
     * Whether to skip this validator if the input is empty
     * @var bool
     */
    public $skipOnEmpty = false;

    public function init()
    {
        parent::init();

        if ($this->message === null) {
            $this->message = SW::t('yii', 'The verification code is incorrect.');
        }
    }

    /**
     * @name \yii\base\Model $model
     * @name string $attribute
     * @name \yii\web\View $view
     * @return string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = SW::t('yii', '{attribute} cannot be blank.',
            ['attribute' => $model->getAttributeLabel($attribute)]
        );
        return "(function(messages){if(!grecaptcha.getResponse()){messages.push('{$message}');}})(messages);";
    }

    /**
     * @name string $value
     * @return array|null
     * @throws Exception
     */
    protected function validateValue($value)
    {
        if (!SettingModel::get('recaptcha_secret')) {
            throw new InvalidConfigException('Required `recatpcha_secret` setting isn\'t set.');
        }

        if (empty($value)) {
            if (!($value = SW::$app->request->post(self::CAPTCHA_RESPONSE_FIELD))) {
                return [$this->message, []];
            }
        }

        $request = self::SITE_VERIFY_URL . '?' . http_build_query(
                [
                    'secret' => SettingModel::get('recaptcha_secret'),
                    'response' => $value,
                    'remoteip' => SW::$app->request->userIP,
                ]
            );
        $response = $this->getResponse($request);
        if (!isset($response['success'])) {
            throw new Exception('Invalid recaptcha verify response.');
        }
        return $response['success'] ? null : [$this->message, []];
    }

    /**
     * @name string $request
     * @return mixed
     */
    protected function getResponse($request)
    {
        $response = file_get_contents($request);
        return Json::decode($response, true);
    }
}