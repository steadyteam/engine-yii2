<?php

namespace Steady\Engine\Validators;

use Steady\Engine\Base\Model;
use Steady\Engine\SW;
use yii\validators\Validator;

class SlugValidator extends Validator
{
    const SLUG_PATTERN = '/^[0-9a-z-]{0,128}$/';

    const SLUG_MESSAGE_RULES = 'Slug can contain only 0-9, a-z and "-" characters (max: 128)';

    /**
     * @var string
     */
    public $pattern = self::SLUG_PATTERN;

    /**
     * @name Model $model
     * @name string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match($this->pattern, $model->$attribute)) {
            $this->addError($model, $attribute, SW::t('admin', SlugValidator::SLUG_MESSAGE_RULES));
        }
    }
}