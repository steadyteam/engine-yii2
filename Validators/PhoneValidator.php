<?php

namespace Steady\Engine\Validators;

use Steady\Engine\Base\Model;
use yii\validators\Validator;

class PhoneValidator extends Validator
{
    const PHONE_PATTERN = '/^[0-9a-z-\\s]{5,16}$/';

    /**
     * @var string
     */
    public $pattern = self::PHONE_PATTERN;

    /**
     * @name Model $model
     * @name string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match($this->pattern, $model->$attribute)) {
            $name = $model->getAttributeLabel($attribute);
            $this->addError($model, $attribute, "$name может содержать только цифры и -");
        }
    }
}