<?php

namespace Steady\Engine\Validators;

use Steady\Engine\Base\Model;
use yii\validators\Validator;

class CyrillicValidator extends Validator
{
    const CYRILLIC_PATTERN = "/[а-я]/u";

    /**
     * @var string
     */
    public $pattern = self::CYRILLIC_PATTERN;

    /**
     * @name Model $model
     * @name string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match($this->pattern, $model->$attribute)) {
            $name = $model->getAttributeLabel($attribute);
            $this->addError($model, $attribute, "$name может содержать только символы кириллицы");
        }
    }
}