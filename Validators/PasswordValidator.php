<?php

namespace Steady\Engine\Validators;

use Steady\Engine\Base\Model;
use yii\validators\Validator;

class PasswordValidator extends Validator
{
    const LEN_MIN = 6;

    const LEN_MAX = 20;

    const PATTERN = '/^[0-9a-z-_\/]{' . self::LEN_MIN . ',' . self::LEN_MAX . '}$/';

    const MESSAGE_MIN = 'Пароль должен содержать не менее ' . self::LEN_MIN . ' символов';

    const MESSAGE_MAX = 'Пароль может содержать не более ' . self::LEN_MAX . ' символов';

    const MESSAGE_RULES = 'Пароль может содержать цифры, английские буквы и спец. символы';

    /**
     * @var string
     */
    public $pattern = self::PATTERN;

    /**
     * @name Model $model
     * @name string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $model->$attribute = filter_var($model->$attribute, FILTER_SANITIZE_STRING);

        if (strlen($model->$attribute) < self::LEN_MIN) {
            $this->addError($model, $attribute, self::MESSAGE_MIN);
        } else if (strlen($model->$attribute) > self::LEN_MAX) {
            $this->addError($model, $attribute, self::MESSAGE_MAX);
        } else if (!preg_match($this->pattern, $model->$attribute)) {
            $this->addError($model, $attribute, self::MESSAGE_RULES);
        }
    }
}