<?php

namespace Steady\Engine\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\helpers\ArrayHelper;

/**
 * @property int tag_assign_id
 * @property int tag_id
 * @property int owner_id
 * @property string class_alias
 */
class TagAssignModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'tags_assign';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'tag_assign_id' => $migration->primaryKey(),
            'tag_id' => $migration->integer(11)->notNull(),
            'owner_id' => $migration->integer(11)->notNull(),
            'class_alias' => $migration->string(128)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['tag_id', 'integer'],
            ['tag_id', 'required'],
            ['owner_id', 'integer'],
            ['owner_id', 'required'],
            ['class_alias', SlugAdvancedValidator::class],
            ['class_alias', 'required'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }
}