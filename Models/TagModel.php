<?php

namespace Steady\Engine\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;

/**
 * @property int tag_id
 * @property string name
 * @property int frequency
 */
class TagModel extends AdvancedModel
{
    public function init()
    {
        parent::init();
    }

    public static function tableName()
    {
        return 'tags';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'tag_id' => $migration->primaryKey(),
            'name' => $migration->string(64)->notNull()->unique(),
            'frequency' => $migration->integer(11)->null(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['name', 'string', 'max' => 64],
            ['name', 'required'],
            ['frequency', 'integer'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'name' => SW::t('admin', 'Name'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }
}