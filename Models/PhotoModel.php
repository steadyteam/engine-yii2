<?php

namespace Steady\Engine\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\SortableModelBehavior;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\helpers\ArrayHelper;

/**
 * @property int photo_id
 * @property int owner_id
 * @property int class_alias
 * @property string image
 * @property string description
 *
 * @mixin SortableModelBehavior
 */
class PhotoModel extends AdvancedModel
{
    const PHOTO_MAX_WIDTH = 1900;
    const PHOTO_THUMB_WIDTH = 120;
    const PHOTO_THUMB_HEIGHT = 90;

    public function behaviors()
    {
        $array = [
            //'sort' => SortableModelBehavior::class
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public static function tableName()
    {
        return 'photos';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'photo_id' => $migration->primaryKey(),
            'owner_id' => $migration->integer(11)->notNull(),
            'class_alias' => $migration->string(128)->notNull(),
            'image' => $migration->string(1024)->notNull(),
            'description' => $migration->string(256),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['owner_id', 'integer'],
            ['owner_id', 'required'],
            ['class_alias', SlugAdvancedValidator::class],
            ['class_alias', 'required'],
            ['image', 'image'],
            ['description', 'string'],
            ['description', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function afterDelete()
    {
        parent::afterDelete();

        @unlink(SW::getAlias('@root') . $this->image);
    }
}