<?php

namespace Steady\Engine\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Validators\EscapeValidator;
use Steady\Engine\Validators\SlugAdvancedValidator;

/**
 * @property int seotext_id
 * @property int owner_id
 * @property string class_alias
 * @property string h1
 * @property string title
 * @property string keywords
 * @property string description
 * @property string canonical
 */
class SeoTextModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'seotext';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'seotext_id' => $migration->primaryKey(),
            'owner_id' => $migration->integer(11)->notNull(),
            'class_alias' => $migration->string(128)->notNull(),
            'h1' => $migration->string(256)->notNull(),
            'title' => $migration->string(256)->notNull(),
            'keywords' => $migration->string(256)->notNull(),
            'description' => $migration->string(256)->notNull(),
            'canonical' => $migration->string(256)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        //$migration->createIndexNamed(static::tableName(), ['class', 'item_id'], true);
        //$migration->addForeignKeyNamed(static::tableName(), 'layout_id', LayoutModel::tableName(), 'layout_id');

        return $success;
    }

    public function rules()
    {
        return [
            [['h1', 'title', 'keywords', 'description', 'canonical'], 'trim'],
            [['h1', 'title', 'keywords', 'description', 'canonical'], 'string', 'max' => 256],
            [['h1', 'title', 'keywords', 'description', 'canonical'], EscapeValidator::class],
            //['canonical', SlugAdvancedValidator::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'h1' => 'H1',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'canonical' => 'Canonical',
        ];
    }

    public function isEmpty()
    {
        return (!$this->h1 && !$this->title && !$this->keywords && !$this->description);
    }
}