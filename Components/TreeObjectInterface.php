<?php

namespace Steady\Engine\Components;

interface TreeObjectInterface
{
    public function parent();

    public function parents();

    public function brothers($andSelf);

    public function children();

    public function descendants($depth, $andSelf);
}