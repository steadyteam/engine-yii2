<?php

namespace Steady\Engine\Components;

use paulzi\jsonBehavior\JsonBehavior;
use paulzi\jsonBehavior\JsonValidator;
use Steady\Engine\Base\Migration;
use Steady\Engine\Base\Model;
use Steady\Engine\Behaviors\PhotoBehavior;
use Steady\Engine\Helpers\CacheHelper;
use Steady\Engine\Modules\Dictionary\Models\DictionaryModel;
use Steady\Engine\Modules\Field\Behaviors\FieldableBehavior;
use Steady\Engine\SW;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * @property int type
 * @property integer access_level
 * @property integer status
 * @property boolean active
 * @property boolean hidden
 *
 * @property string created_at
 * @property string updated_at
 * @property int created_by
 * @property int updated_by
 * @property array params
 *
 * @mixin FieldableBehavior
 * @mixin PhotoBehavior
 * @mixin TimestampBehavior
 * @mixin BlameableBehavior
 */
abstract class AdvancedModel extends Model
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public function behaviors()
    {
        $array = [
            'fieldable' => FieldableBehavior::class,
            'photos' => PhotoBehavior::class,
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'defaultValue' => 0,
            ],
            'json' => [
                'class' => JsonBehavior::class,
                'attributes' => ['params'],
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            if (!$this->type) {
                $this->type = 1;
            }
            $this->access_level = 1;
            $this->status = 1;
            $this->active = true;
            $this->hidden = false;
        }
    }

    public static function migrationUp(Migration $migration, array $columns): bool
    {
        $columns = ArrayHelper::merge($columns, [
            'type' => $migration->integer(3)->notNull()->defaultValue(1),
            'access_level' => $migration->integer(3)->notNull()->defaultValue(1),
            'status' => $migration->integer(3)->notNull()->defaultValue(1),
            'active' => $migration->boolean()->notNull()->defaultValue(true),
            'hidden' => $migration->boolean()->notNull()->defaultValue(false),
            'created_at' => $migration->dateTime()->defaultExpression('NOW()'),
            'updated_at' => $migration->dateTime()->defaultExpression('NOW()'),
            'created_by' => $migration->integer(11)->notNull()->defaultValue(0),
            'updated_by' => $migration->integer(11)->notNull()->defaultValue(0),
            'params' => $migration->text(),
        ]);

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['type', 'integer'],
            ['type', 'default', 'value' => 1],
            ['access_level', 'integer'],
            ['access_level', 'default', 'value' => 1],
            ['status', 'integer'],
            ['status', 'default', 'value' => 1],
            ['active', 'boolean'],
            ['active', 'default', 'value' => true],
            ['hidden', 'boolean'],
            ['hidden', 'default', 'value' => false],
            ['params', JsonValidator::class, 'merge' => true],
            //['params', 'safe'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'type' => SW::t('admin', 'Type'),
            'access_level' => SW::t('admin', 'Access level'),
            'status' => SW::t('admin', 'Status'),
            'active' => SW::t('admin', 'Active'),
            'hidden' => SW::t('admin', 'Hidden'),
            'params' => SW::t('admin', 'Params'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public function dictionaries()
    {
        return CacheHelper::cache($this->getAlias() . '_dictionary', 3600, function () {

            $dictionaries = [];

            /** @var DictionaryModel $modelDictionary */
            $modelDictionary = DictionaryModel::find()->where(['alias' => $this->getAlias()])->one();

            if ($modelDictionary instanceof DictionaryModel) {
                $dictionaries = $modelDictionary->getChildren()->asArray()->all();
            }

            return $dictionaries;
        });
    }

    /**
     * @return array
     */
    public function types(): array
    {
        return CacheHelper::cache($this->getAlias() . '_types', 3600, function () {

            $types = [];

            /** @var DictionaryModel $typeDictionary */
            $typeDictionary = DictionaryModel::find()->where(['alias' => $this->getAlias() . '_type'])->one();

            if ($typeDictionary instanceof DictionaryModel) {
                $children = $typeDictionary->getChildren()->asArray()->all();
                if ($children) {
                    foreach ($children as $type) {
                        $types[$type['value']] = $type['text'];
                    }
                }
            }


            return $types;
        });
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $result = parent::load($data, $formName);
        if ($result) {
            if (isset($data['Fields']) && !empty($data['Fields']) && is_array($data['Fields'])) {
                $this->setFieldsArray($data['Fields']);
            }
        }
        return $result;
    }
}