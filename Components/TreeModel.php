<?php

namespace Steady\Engine\Components;

use paulzi\adjacencyList\AdjacencyListBehavior;
use paulzi\autotree\AutoTreeTrait;
use paulzi\nestedsets\NestedSetsBehavior;
use Steady\Engine\Base\Migration;
use Steady\Engine\SW;
use yii\base\InvalidValueException;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * @property int parent_id
 * @property int sort
 * @property int lft
 * @property int rgt
 * @property int depth
 *
 * @mixin AdjacencyListBehavior
 * @mixin NestedSetsBehavior
 */
abstract class TreeModel extends AdvancedModel implements TreeInterface
{
    use AutoTreeTrait {
        appendTo as protected traitAppendTo;
    }

    public function behaviors()
    {
        $array = [
            ['class' => AdjacencyListBehavior::class],
            ['class' => NestedSetsBehavior::class],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->parent_id = 1;
        }
    }

    public static function migrationUp(Migration $migration, array $columns): bool
    {
        $columns = ArrayHelper::merge($columns, [
            'parent_id' => $migration->integer(11),
            'sort' => $migration->integer(11)->notNull(),
            'lft' => $migration->integer(11)->notNull(),
            'rgt' => $migration->integer(11)->notNull(),
            'depth' => $migration->integer(11)->notNull(),
        ]);

        $success = parent::migrationUp($migration, $columns);

        /*reset($columns);
        $primaryName = key($columns);
        $migration->addForeignKeyNamed(static::tableName(), 'parent_id', static::tableName(), $primaryName);*/

        // Вставка base записи. Корень дерева обязателен для работы системы
        static::insertRoot();

        return $success;
    }

    public function rules()
    {
        $array = [
            ['parent_id', 'integer'],
            ['parent_id', 'default', 'value' => 1],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @name bool $runValidation
     * @name null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->parent_id && ($this->isNewRecord || $this->parent_id != $this->oldAttributes['parent_id'])) {
            $this->appendTo($this->parent_id);
        } else if (!$this->parent_id && $this->isEmptyTree()) {
            $this->makeRoot();
        } else if (!$this->parent_id) {
            throw new InvalidValueException('Field `parent_id` is empty!');
        }

        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @name $id_node
     * @return $this
     */
    public function appendTo($id_node)
    {
        if ($id_node instanceof TreeModel) {
            $node = $id_node;
        } else {
            $node = static::findOne([static::primaryKey()[0] => $id_node]);
        }

        return $this->traitAppendTo($node);
    }

    /**
     * @return array|null|TreeModel
     */
    public function parent()
    {
        return $this->getParent()->one();
    }

    /**
     * @return array|TreeModel
     */
    public function parents()
    {
        return $this->getParents()->all();
    }

    /**
     * @name bool $andSelf
     * @return array|TreeModel
     */
    public function brothers($andSelf = false)
    {
        /** @var TreeModel $result */
        $query = $this->parent()->getChildren();

        if (!$andSelf) {
            $query->andWhere(['!=', static::primaryKey()[0], $this->primaryKey]);
        }

        $brothers = $query->all();

        return $brothers;
    }

    /**
     * @return array|TreeModel
     */
    public function children()
    {
        return $this->getChildren()->all();
    }

    /**
     * @name null $depth
     * @name null $andSelf
     * @return array|TreeModel
     */
    public function descendants($depth = null, $andSelf = null)
    {
        return $this->getDescendants($depth, $andSelf)->all();
    }

    /**
     * @return array|TreeModel
     * @throws Exception
     */
    public function validParents()
    {
        $tree = self::tree(true);
        $descendants = $this->descendants(null, true);

        foreach ($tree as $key => $node) {
            foreach ($descendants as $key2 => $child) {
                if ($node->primaryKey == $child->primaryKey) {
                    unset($tree[$key]);
                    break;
                }
            }
        }

        return $tree;
    }

    /**
     * @return bool
     */
    public function up()
    {
        return $this->move('up');
    }

    /**
     * @return bool
     */
    public function down()
    {
        return $this->move('down');
    }

    /**
     * @name string $direction
     * @return bool
     */
    private function move(string $direction): bool
    {
        $up = $direction == 'up';
        $node = static::find()
            ->where(['and', ['depth' => $this->depth], [($up ? '<' : '>'), 'lft', $this->lft]])
            ->orderBy(['lft' => ($up ? SORT_DESC : SORT_ASC)])
            ->one();

        if ($node) {
            if ($up) {
                $this->insertBefore($node);
            } else {
                $this->insertAfter($node);
            }
            $node->save();
            $this->save();

            return true;
        }

        return false;
    }

    /**
     * @return null|static
     * @throws Exception
     */
    public static function root()
    {
        $model = static::findOne(['parent_id' => null]);
        if (!$model) {

            $model = self::createRoot();
            if (!$model) {
                throw new Exception('Root not found in ' . static::class . ' class');
            }
        }

        return $model;
    }

    /**
     * @name bool $andSelf
     * @return TreeModel[]
     * @throws Exception
     */
    public static function tree($andSelf = false)
    {
        $result = [];

        $root = static::root();
        if ($andSelf) {
            $result[] = $root;
        }
        $descendants = $root->descendants();

        return ArrayHelper::merge($result, $descendants);
    }

    /**
     * @return TreeModel
     */
    abstract static function insertRoot(): TreeModel;

    /**
     * @return TreeModel
     * @throws Exception
     */
    public static function createRoot(): TreeModel
    {
        $model = static::insertRoot();

        if (!$model->save()) {
            throw new Exception('Failed to create root in ' . static::class . ' class', $model->getErrors());
        }

        return $model;
    }

    /**
     * @return bool
     */
    public function isEmptyTree(): bool
    {
        return !($this->find()->exists());
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public static function buildTree()
    {
        $answer = '';

        /** @var TreeModel[] $models */
        $models = self::find()->all();

        $transaction = SW::$app->db->beginTransaction();

        try {
            foreach ($models as $model) {
                if ($model->parent_id) {
                    $parent = self::findOne([self::primaryKey()[0] => $model->parent_id]);
                    try {
                        $model->appendTo($parent);
                        $answer .= "Model $model->primaryKey append to $parent->primaryKey!</br>";
                    } catch (\Exception $e) {
                        throw new \Exception('Model "' . $model->primaryKey . '" can not move to ' . $model->primaryKey);
                    }
                } else if (!$model->isRoot()) {
                    try {
                        $model->makeRoot();
                        $answer .= "Model $model->primaryKey make root!</br>";
                    } catch (\Exception $e) {
                        throw new \Exception('Model "' . $model->primaryKey . '" can not make root');
                    }
                }
                $saved = $model->save();
                if (!$saved) {
                    throw new \Exception('Model not saved: ' . $model->formatErrors());
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        echo $answer;
        return true;
    }

    public static function reorder($node)
    {
        $result = [];
        foreach ($node->children as $child) {
            $result[] = $child;
            $result = array_merge($result, self::reorder($child));
        }
        return $result;
    }
}