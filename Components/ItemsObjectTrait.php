<?php

namespace Steady\Engine\Components;

use Steady\Engine\Base\Api;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Base\Model;
use Steady\Engine\Exceptions\NotImplementedException;
use yii\data\ActiveDataProvider;

trait ItemsObjectTrait
{
    /**
     * @var ApiObject[]
     */
    private $_items;

    /**
     * @name $id_alias
     * @return ApiObject
     */
    protected function item($id_alias)
    {
        if (!isset($this->_items[$id_alias])) {
            $this->_items[$id_alias] = $this->findItem($id_alias);
        }
        return $this->_items[$id_alias];
    }

    /**
     * @name array $options
     * @return ApiObject[]
     */
    protected function items($options = [])
    {
        if (!empty($options['query'])) {
            $query = $options['query'];
        } else {
            $itemModel = $this->getItemModelClass();
            // TODO: Стоит поставить проверку на наличие подобных методов
            $query = $itemModel::find()->with(['seo', 'category']);
        }

        if (!empty($options['pagination'])) {
            $this->_adp = new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'title' => SORT_ASC,
                    ],
                ],
                'pagination' => $options['pagination'],
            ]);
            $models = $this->_adp->models;
        } else {
            $models = $query->all();
        }

        $items = [];
        /** @var Model $model */
        foreach ($models as $model) {
            $items[] = $this->createItemObject($model);
        }

        return $items;
    }

    /**
     * @name $id_alias
     * @return ApiObject|null
     */
    protected function findItem($id_alias)
    {
        /** @var Model $class */
        $class = $this->getItemModelClass();
        $pk = $class::primaryKey()[0];

        $condition = ['or'];

        if (is_numeric($id_alias)) {
            $condition[] = "$pk=:id_alias";
        } else {
            if (isset(class_implements($class)[Aliasable::class])) {
                $condition[] = 'alias=:id_alias';
            }
            if (isset(class_implements($class)[Slugable::class])) {
                $condition[] = 'slug=:id_alias';
            }
        }
        /** @var Model $model */
        $model = $class::find()
            ->where($condition, [':id_alias' => $id_alias])
            ->one();

        return $model ? $this->createItemObject($model) : null;
    }

    /**
     * @name Model $model
     * @return ApiObject
     */
    protected function createItemObject(Model $model): ApiObject
    {
        $class = $this->getItemObjectClass();
        $object = new $class($this->getItemApi(), $model);
        return $object;
    }

    /**
     * @return Api
     */
    protected function getItemApi()
    {
        if ($this instanceof Api) {
            return $this;
        } else if ($this instanceof ApiObject) {
            return $this->api;
        } else {
            throw new NotImplementedException(get_class($this));
        }
    }

    /**
     * @return ApiObject
     */
    protected function getItemObject()
    {
        /** @var ApiObject $object */
        $object = $this->itemObjectName;
        if (!$object instanceof ApiObject) throw new NotImplementedException(get_class($object));

        return $object;
    }

    /**
     * @return Model
     */
    protected function getItemModelClass()
    {
        /** @var Model $class */
        $class = $this->itemModelName;
        if (!is_subclass_of($class, Model::class)) throw new NotImplementedException($class);

        return $class;
    }

    /**
     * @return ApiObject
     */
    protected function getItemObjectClass()
    {
        /** @var ApiObject $class */
        $class = $this->itemObjectName;
        if (!is_subclass_of($class, ApiObject::class)) throw new NotImplementedException($class);

        return $class;
    }
}
