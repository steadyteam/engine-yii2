<?php

namespace Steady\Engine\Components;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Exceptions\NotImplementedException;

trait TreeApiTrait
{
    /**
     * @name $id_alias
     * @return ApiObject|null
     */
    public function parent($id_alias): ?ApiObject
    {
        return $this->getTreeObject($id_alias)->parent();
    }

    /**
     * @name $id_alias
     * @return ApiObject[]
     */
    public function parents($id_alias)
    {
        return $this->getTreeObject($id_alias)->parents();
    }

    /**
     * @name $id_alias
     * @name bool $andSelf
     * @return ApiObject[]
     */
    public function brothers($id_alias, $andSelf = false)
    {
        return $this->getTreeObject($id_alias)->brothers($andSelf);
    }

    /**
     * @name $id_alias
     * @return ApiObject[]
     */
    public function children($id_alias)
    {
        return $this->getTreeObject($id_alias)->children();
    }

    /**
     * @name $id_alias
     * @name null $depth
     * @name null $andSelf
     * @return ApiObject[]
     */
    public function descendants($id_alias, $depth = null, $andSelf = null)
    {
        return $this->getTreeObject($id_alias)->descendants($depth, $andSelf);
    }

    /**
     * @name bool $andSelf
     * @return ApiObject[]
     * @throws \yii\db\Exception
     */
    public function tree($andSelf = false)
    {
        $class = $this->getModelClass();

        return $this->createObjectsFromModels($class::tree($andSelf));
    }

    /**
     * @param $id_alias
     * @param null $depth
     * @param null $andSelf
     * @param int $sort
     * @return ApiObject[]
     */
    public function descendantsOrderByDate($id_alias, $depth = null, $andSelf = null, $sort = SORT_DESC)
    {
        $models = $this->getTreeObject($id_alias)->model->getDescendants($depth, $andSelf)
            ->orderBy(['updated_at' => $sort])
            ->all();

        return $this->createObjectsFromModels($models);
    }

    /**
     * @name $id_alias
     * @return ApiObject|TreeObjectTrait
     */
    public function getTreeObject($id_alias): ApiObject
    {
        $object = parent::get($id_alias);
        if (!$object instanceof TreeObjectInterface) throw new NotImplementedException(get_class($object));

        return $object;
    }

    /**
     * @return TreeModel
     */
    public function getModelClass()
    {
        $class = parent::getModelClass();
        if (!is_subclass_of($class, TreeModel::class)) throw new NotImplementedException($class);

        return $class;
    }
}