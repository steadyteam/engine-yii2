<?php

namespace Steady\Engine\Components;

use Steady\Engine\Base\Model;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\validators\Validator;

/**
 * @property Model $owner
 */
abstract class RulesBehavior extends Behavior
{
    /**
     * @var Validator[] track references of appended validators
     */
    protected $validators = [];

    /**
     * Added rules to $owner
     * @return mixed
     */
    abstract public function rules(): array;

    /**
     * @inheritdoc
     * @name Model $owner
     * @throws InvalidConfigException
     */
    public function attach($owner)
    {
        parent::attach($owner);
        $validators = $owner->validators;
        foreach ($this->rules() as $rule) {
            if ($rule instanceof Validator) {
                $validators->append($rule);
                $this->validators[] = $rule; // keep a reference in behavior
            } else if (is_array($rule) && isset($rule[0], $rule[1])) { // attributes, validator type
                $validator = Validator::createValidator($rule[1], $owner, (array)$rule[0], array_slice($rule, 2));
                $validators->append($validator);
                $this->validators[] = $validator; // keep a reference in behavior
            } else {
                throw new InvalidConfigException('Invalid validation rule: a rule must specify both 
                    attribute names and validator type.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function detach()
    {
        $ownerValidators = $this->owner->validators;
        $cleanValidators = [];
        foreach ($ownerValidators as $validator) {
            if (!in_array($validator, $this->validators)) {
                $cleanValidators[] = $validator;
            }
        }
        $ownerValidators->exchangeArray($cleanValidators);
    }
}