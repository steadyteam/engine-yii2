<?php

namespace Steady\Engine\Components;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Base\Model;

trait ItemsApiTrait
{
    use ItemsObjectTrait {
        item as itemTrait;
        items as itemsTrait;
    }

    /**
     * @var ApiObject[]
     */
    private $_cats = [];

    /**
     * @name string|int $id_alias
     * @return null|ApiObject|self
     */
    public function get($id_alias)
    {
        return $this->cat($id_alias);
    }

    /**
     * @name string|int $id_alias
     * @return null|ApiObject
     */
    public function cat($id_alias): ?ApiObject
    {
        if (!isset($this->_cats[$id_alias])) {
            $this->_cats[$id_alias] = $this->findCategory($id_alias);
        }
        return $this->_cats[$id_alias];
    }

    /**
     * @name string|int $id_alias
     * @return null|ApiObject
     */
    public function item($id_alias): ?ApiObject
    {
        return $this->itemTrait($id_alias);
    }

    /**
     * @name array $options
     * @return ApiObject[]
     */
    public function items($options = [])
    {
        return $this->itemsTrait($options);
    }

    /**
     * @return int|string
     */
    public function itemsTotalCount()
    {
        $class = $this->getItemModelClass();

        return $class::find()->count();
    }

    /**
     * @name string|int $id_alias
     * @return ApiObject|null
     */
    protected function findCategory($id_alias): ?ApiObject
    {
        /** @var Model $class */
        $class = $this->getModelClass();
        /** @var Model $model */
        $model = $class::find()
            ->where($this->getConditionForFind($id_alias), [':id_alias' => $id_alias])
            //->status(GalleryModel::STATUS_ON)
            ->one();

        return $model ? $this->createObject($model) : null;
    }
}
