<?php

namespace Steady\Engine\Components;

use Steady\Engine\Base\Api;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Exceptions\NotImplementedException;

/**
 * @property Api|TreeApiTrait api
 * @property TreeModel model
 */
trait TreeObjectTrait
{
    public $parent_id;
    public $depth;

    /**
     * @return ApiObject
     */
    public function parent(): ?ApiObject
    {
        return $this->getApi()->createObject($this->getTreeModel()->parent());
    }

    /**
     * @return ApiObject[]
     */
    public function parents()
    {
        return $this->getApi()->createObjectsFromModels($this->getTreeModel()->parents());
    }

    /**
     * @name bool $andSelf
     * @return ApiObject[]
     */
    public function brothers($andSelf = false)
    {
        return $this->getApi()->createObjectsFromModels($this->getTreeModel()->brothers($andSelf));
    }

    /**
     * @return ApiObject[]
     */
    public function children()
    {
        return $this->getApi()->createObjectsFromModels($this->getTreeModel()->children());
    }

    /**
     * @name null $depth
     * @name null $andSelf
     * @return ApiObject[]
     */
    public function descendants($depth = null, $andSelf = null)
    {
        return $this->getApi()->createObjectsFromModels($this->getTreeModel()->descendants($depth, $andSelf));
    }

    /**
     * @return Api
     */
    protected function getApi()
    {
        if (!$this->api instanceof Api) throw new NotImplementedException(get_class($this->api));

        return $this->api;
    }

    /**
     * @return TreeModel
     */
    protected function getTreeModel()
    {
        if (!$this->model instanceof TreeModel) throw new NotImplementedException(get_class($this->model));

        return $this->model;
    }
}