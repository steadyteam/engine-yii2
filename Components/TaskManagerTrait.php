<?php

namespace Steady\Engine\Components;

trait TaskManagerTrait
{
    /**
     * @var Task[]
     */
    private $_tasks = [];

    /**
     * @name string $name
     * @name callable $action
     * @return Task
     */
    public function task(string $name, callable $action = null): Task
    {
        $this->_tasks[$name] = new Task(['name' => $name, 'active' => true, 'action' => $action]);

        return $this->_tasks[$name];
    }

    /**
     * @name string $name
     * @return null|Task
     */
    public function directive(string $name): ?Task
    {
        $task = $this->_tasks[$name] ?? null;
        if ($task && $task instanceof Task && $task->active) {
            return $task;
        }
        return null;
    }
}