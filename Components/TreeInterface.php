<?php

namespace Steady\Engine\Components;

interface TreeInterface
{
    public function parent();

    public function parents();

    public function brothers();

    public function children();

    public function descendants();

    public function validParents();

    public function up();

    public function down();

    public static function root();

    public static function tree();
}