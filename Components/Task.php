<?php

namespace Steady\Engine\Components;

use yii\base\BaseObject;

class Task extends BaseObject
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var null|callable
     */
    public $action = null;

    /**
     * @var bool
     */
    public $active = true;

    /**
     * @var array
     */
    public $data = [];

    /**
     * @var mixed
     */
    protected $result;


    /**
     * @name array $data
     * @return $this
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return $this
     */
    public function realization()
    {
        if ($this->action && is_callable($this->action)) {
            $this->result = call_user_func($this->action);
        }
        $this->active = false;
        return $this;
    }
}