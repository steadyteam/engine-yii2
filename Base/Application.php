<?php

namespace Steady\Engine\Base;

use Steady\Engine\Service\ApiService;

/**
 * @property ApiService api
 */
class Application extends \yii\web\Application
{
    use ModuleTrait;
}