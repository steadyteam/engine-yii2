<?php

namespace Steady\Engine\Base;

use Steady\Engine\SW;

class Migration extends \yii\db\Migration
{
    public $engine = 'ENGINE=MyISAM DEFAULT CHARSET=utf8';

    /**
     * @param $table
     * @param $columns
     * @param bool $unique
     */
    public function createIndexNamed($table, $columns, $unique = false)
    {
        $name = $table . '_' . (is_array($columns) ? implode('_', $columns) : $columns) . '_index';

        parent::createIndex($name, $table, $columns, $unique);
    }

    /**
     * @param $table
     * @param $columns
     * @param $refTable
     * @param $refColumns
     * @param null $delete
     * @param null $update
     */
    public function addForeignKeyNamed($table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        $name = $table . '_' . (is_array($refColumns) ? implode('_', $refColumns) : $refColumns) . '_fk';

        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * @param $table
     * @param $column
     * @param $type
     */
    public function addColumnEx($table, $column, $type)
    {
        // TODO: Возможно стоит заменить проверку на установку. Учесть что запрос выполняется в транзакции.
        if (!$this->existsColumn($table, $column) && INSTALLED) {
            parent::addColumn($table, $column, $type);
        }
    }


    /**
     * @param string $table
     * @param string $column
     * @return bool
     */
    public function existsColumn(string $table, string $column): bool
    {
        $table = SW::$app->db->schema->getTableSchema($table);
        if (!isset($table->columns[$column])) {
            return true;
        }
        return false;
    }
}