<?php

namespace Steady\Engine\Base;

use yii\db\ActiveQuery;

class Query extends ActiveQuery
{
    /**
     * Apply condition by status
     * @name int $status
     * @return Query
     */
    public function status($status): self
    {
        $this->andWhere(['status' => (int)$status]);
        return $this;
    }

    /**
     * Order by primary key DESC
     * @return Query
     */
    public function desc(): self
    {
        /** @var Model $model */
        $model = $this->modelClass;
        $this->orderBy([$model::primaryKey()[0] => SORT_DESC]);
        return $this;
    }

    /**
     * Order by primary key ASC
     * @return Query
     */
    public function asc(): self
    {
        /** @var Model $model */
        $model = $this->modelClass;
        $this->orderBy([$model::primaryKey()[0] => SORT_ASC]);
        return $this;
    }

    /**
     * Order by order_num
     */
    public function sort(): self
    {
        $this->orderBy(['order_num' => SORT_DESC]);
        return $this;
    }

    /**
     * Order by date
     * @return Query
     */
    public function sortDate(): self
    {
        $this->orderBy(['time' => SORT_DESC]);
        return $this;
    }
}