<?php

namespace Steady\Engine\Base;

use Steady\Admin\Api\PhotoObject;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Exceptions\NotImplementedException;
use Steady\Engine\Helpers\Image;
use Steady\Engine\SW;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\widgets\LinkPager;

abstract class ApiObject extends BaseObject
{
    /**
     * @var string
     */
    public $modelName;

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var ActiveDataProvider
     */
    protected $_adp;

    /**
     * ApiObject constructor.
     * @name Api $api
     * @name Model $model
     * @name array $config
     */
    public function __construct(Api $api, Model $model, array $config = [])
    {
        $this->api = $api;
        $this->model = $model;

        if (!$this->model instanceof $this->modelName) {
            throw new \InvalidArgumentException($model->getAlias() . ' not ' . $this->modelName);
        }

        foreach ($model->attributes as $attribute => $value) {
            if ($this->canSetProperty($attribute)) {
                $this->{$attribute} = $value;
            }
        }

        parent::__construct($config);
    }

    // TODO: Выяснить зачем была переопределенна эта функция, возможно проблемы со свойством модели
    /*public function __get($name)
    {
    }*/

    /**
     * Calls after construct
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->model->primaryKey;
    }


    /**
     * @param null $format
     * @return string
     * @throws InvalidConfigException
     */
    public function getDate($format = 'd MMMM yyyy')
    {
        if ($this->model instanceof AdvancedModel) {
            return SW::$app->formatter->asDate($this->model->created_at, $format);
        }
        return null;
    }

    /**
     * @param $name
     * @return null|string
     */
    public function param($name)
    {
        if ($this->model instanceof AdvancedModel) {
            return $this->model->params[$name] ?? null;
        }
        return null;
    }

    /**
     * @name $alias
     * @return null|string
     */
    public function field($alias)
    {
        if ($this->model instanceof AdvancedModel) {
            return $this->model->fields[$alias] ?? null;
        }
        return null;
    }

    /**
     * @param $alias
     * @return null|string
     * @deprecated Use field()
     * @see ApiObject::field()
     */
    public function fields($alias)
    {
        return $this->field($alias);
    }

    /**
     * @name string $attribute Имя SEO аттрибута: h1, title, description, keywords
     * @name string $default Значение которое возвращается если аттрибут не заполнен или не найден
     * @return string
     */
    public function seo($attribute, $default = '')
    {
        /** @var AdvancedModel|SeoBehavior $model */
        $model = $this->model;
        
        if (!empty($model->seoText->{$attribute})) {
            return $model->seoText->{$attribute};
        } else {
            return $default;
        }
    }

    /**
     * @return array
     */
    public function photos()
    {
        $photos = [];

        if ($this->model instanceof AdvancedModel) {
            foreach ($this->model->photos as $photo) {
                $photos[] = new PhotoObject($this->api, $photo);
            }
        }
        return $photos;
    }

    /**
     * Creates thumb from model->image attribute with specified width and height.
     * @param null $width
     * @param null $height
     * @param bool $crop
     * @param bool $useNoImage
     * @return string
     */
    public function thumb($width = null, $height = null, $crop = true, $useNoImage = true)
    {
        if ($width || $height) {
            $filename = $this->image ?? null;
            return Image::thumb($filename, $width, $height, $crop, $useNoImage);
        }
        return null;
    }

    /**
     * @name array $options
     * @return string
     * @throws \Exception
     */
    public function pagination($options = [])
    {
        return $this->_adp ? LinkPager::widget(array_merge($options, ['pagination' => $this->_adp->pagination])) : '';
    }

    /**
     * @return \yii\data\Pagination|null
     */
    public function getPagination()
    {
        return $this->_adp ? $this->_adp->pagination : null;
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        if (!$this->model instanceof Model) throw new NotImplementedException(get_class($this->model));

        return $this->model;
    }

    /**
     * @return Model
     */
    public function getModelClass(): Model
    {
        $class = get_class($this->model);
        if (!is_subclass_of($class, Model::class)) throw new NotImplementedException($class);

        return $class;
    }
}