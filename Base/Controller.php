<?php

namespace Steady\Engine\Base;

use Steady\Engine\Components\TaskManagerTrait;
use Steady\Engine\SW;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

abstract class Controller extends \yii\web\Controller
{
    use TaskManagerTrait;

    const TASK_CUSTOM_RENDER = 'customRender';

    /**
     * {@inheritdoc}
     * @throws BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        return parent::beforeAction($action);
    }

    /**
     * @name string $view
     * @name array $params
     * @return bool
     */
    public function beforeRender(string $view, $params = []): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function render($view, $params = [])
    {
        self::beforeRender($view, $params);
        return parent::render($view, $params);
    }

    /**
     * @name \yii\base\Model $model
     * @return array
     */
    protected function ajaxValidate(\yii\base\Model $model): array
    {
        SW::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }

}