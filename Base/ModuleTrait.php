<?php

namespace Steady\Engine\Base;

trait ModuleTrait
{
    /**
     * @var string
     */
    private $_controllersFolderName = 'Controllers';

    /**
     * @var string
     */
    private $_viewsFolderName = 'Resources/views';

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->controllerNamespace === null) {
            $class = get_class($this);
            if (($pos = strrpos($class, '\\')) !== false) {
                $this->controllerNamespace = substr($class, 0, $pos) . '\\' . $this->_controllersFolderName;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getViewPath()
    {
        return $this->getBasePath() . DIRECTORY_SEPARATOR . $this->_viewsFolderName;
    }

    /**
     * @return string
     */
    public function getControllersFolderName(): string
    {
        return $this->_controllersFolderName;
    }

    /**
     * @name string $controllersFolderName
     */
    public function setControllersFolderName(string $controllersFolderName)
    {
        $this->_controllersFolderName = $controllersFolderName;
    }

    /**
     * @return string
     */
    public function getViewsFolderName(): string
    {
        return $this->_viewsFolderName;
    }

    /**
     * @name string $viewsFolderName
     */
    public function setViewsFolderName(string $viewsFolderName)
    {
        $this->_viewsFolderName = $viewsFolderName;
    }
}