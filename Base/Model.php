<?php

namespace Steady\Engine\Base;

use Steady\Engine\Helpers\Mail;
use yii\db\ActiveRecord;
use yii\db\Exception;

abstract class Model extends ActiveRecord
{
    public function getAlias(): string
    {
        return static::getClassAlias();
    }

    public static function getClassAlias()
    {
        return static::tableName() ?? static::class;
    }

    /**
     * @return Query
     */
    public static function find(): Query
    {
        return new Query(get_called_class());
    }

    /**
     * @name bool $runValidation
     * @name null $attributeNames
     * @return bool
     * @throws Exception
     */
    public function saveEx($runValidation = true, $attributeNames = null)
    {
        $saved = static::save($runValidation, $attributeNames);

        if (!$saved) {
            // TODO: Временное решение для тестерования, сделать отдельной функцией дебага
            Mail::newInstance()
                ->setFrom('debug@steady-web.com')
                ->setFromName('Steady Debug')
                ->setTo('steady.webstudio@gmail.com')
                ->setSubject('Steady Exception')
                ->setTextBody($this->formatErrors())
                ->send();
            // TODO: Временное решение для тестерования, доработать
            print_r($this->getErrors());
            exit();
            throw new Exception('Model not saved!', $this->getErrors());
        }
        return true;
    }

    /**
     * @name Migration $migration
     * @name array $columns
     * @return bool
     */
    public static function migrationUp(Migration $migration, array $columns): bool
    {
        $migration->createTable(static::tableName(), $columns, $migration->engine);

        return true;
    }

    /**
     * @name Migration $migration
     * @return bool
     */
    public static function migrationDown(Migration $migration): bool
    {
        $migration->dropTable(static::tableName());

        return true;
    }

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors(): string
    {
        $result = '';
        foreach ($this->getErrors() as $attribute => $errors) {
            $result .= implode(" ", $errors) . " ";
        }
        return $result;
    }
}