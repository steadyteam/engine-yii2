<?php

namespace Steady\Engine\Base;

abstract class Module extends \yii\base\Module
{
    use ModuleTrait;
}