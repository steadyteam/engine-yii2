<?php

namespace Steady\Engine\Base;

use Steady\Admin\Components\SteadyModule;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\Slugable;
use Steady\Engine\Exceptions\NotImplementedException;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\widgets\LinkPager;

abstract class Api extends BaseObject
{
    /**
     * @var string
     */
    public $moduleName;

    /**
     * @var string
     */
    public $modelName;

    /**
     * @var string
     */
    public $objectName;

    /**
     * @var ApiObject[]
     */
    protected $_objects = [];

    /**
     * Last Object
     * @var ApiObject
     */
    protected $_last;

    /**
     * @var ActiveDataProvider
     */
    protected $_adp;

    /**
     * @throws \ReflectionException
     */
    public function init()
    {
        parent::init();

        $this->moduleName = SteadyModule::getModuleName(static::class);
        SteadyModule::registerTranslations($this->moduleName);
    }

    /**
     * @name $id_alias
     * @return ApiObject
     */
    public function get($id_alias)
    {
        if (!isset($this->_objects[$id_alias])) {
            $this->_objects[$id_alias] = $this->findObject($id_alias);
        }
        return $this->_objects[$id_alias];
    }

    /**
     * @name int $limit
     * @name null $where
     * @return ApiObject|ApiObject[]|null
     */
    public function last($limit = 1, $where = null)
    {
        if ($limit === 1 && $this->_last) {
            return $this->_last;
        }

        $result = [];

        $class = $this->getModelClass();
        $query = $class::find()
            //->with('seo')
            ->sortDate()
            ->limit($limit);
        if ($where) {
            $query->andFilterWhere($where);
        }

        /** @var Model $model */
        foreach ($query->all() as $model) {
            $result[] = $this->createObject($model);
        }

        if ($limit > 1) {
            return $result;
        } else {
            $this->_last = count($result) ? $result[0] : null;
            return $this->_last;
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function pagination()
    {
        return $this->_adp ? LinkPager::widget(['pagination' => $this->_adp->pagination]) : '';
    }

    /**
     * @deprecated since 1.1.0 Use pagination()
     * @return null|Pagination
     */
    protected function getPagination()
    {
        return $this->_adp ? $this->_adp->pagination : null;
    }

    /**
     * @name $id_alias
     * @return ApiObject
     */
    protected function findObject($id_alias)
    {
        $class = $this->getModelClass();
        /** @var Model $model */
        $model = $class::find()->where($this->getConditionForFind($id_alias), [':id_alias' => $id_alias])->one();

        return $model ? $this->createObject($model) : $this->notFound($id_alias);
    }

    /**
     * Создает объект API на основе переданной модели из аргумента $model,
     * по умолчанию классом модели является классом указанным в API как аргумент $modelName
     * Для создания объекта, из модели, которая не указана в API как основная (аргумент $modelName),
     * необходимо передать класс создаваемого объекта в аргументе $objectClass,
     * если класс модели не совпадает с переданным классом объекта, выводим исключение
     * @name Model $model
     * @name ApiObject|null $objectClass Класс создаваемого объекта, необходим для проверки соответсвия
     * @return ApiObject
     */
    public function createObject(Model $model, $objectClass = null): ApiObject
    {
        if ($objectClass) {
            if (!is_subclass_of($objectClass, ApiObject::class)) {
                throw new NotImplementedException($objectClass);
            }
        } else {
            $modelClass = $this->getModelClass();
            if (!$model instanceof $modelClass) {
                throw new \InvalidArgumentException($model->getAlias() . ' not ' . $modelClass);
            }
            $objectClass = $this->getObjectClass();
        }
        $object = new $objectClass($this, $model);
        return $object;
    }

    /**
     * @name Model[] $models
     * @name Model|null $class
     * @return ApiObject[]
     */
    public function createObjectsFromModels(array $models, $class = null)
    {
        $objects = [];
        if (!empty($models)) {
            foreach ($models as $model) {
                $objects[] = $this->createObject($model, $class);
            }
        }

        return $objects;
    }

    /**
     * @name $id_alias
     * @return ApiObject
     */
    protected function notFound($id_alias)
    {
        // TODO: Непонятно зачем это было нужно, где-то возможны баги
        /* $class = $this->getModelClass();*/
        /** @var Model $model */
        /*$model = new $class();
        if ($model instanceof Aliasable) {
            $model->alias = $id_alias;
        }
        if ($model instanceof Slugable) {
            $model->slug = $id_alias;
        }
        return $this->createObject($model);*/

        return null;
    }

    protected function getConditionForFind($id_alias): array
    {
        $class = $this->getModelClass();
        $pk = $class::primaryKey()[0];

        $condition = ['or'];

        if (is_numeric($id_alias)) {
            $condition[] = "$pk=:id_alias";
        } else {
            if (isset(class_implements($class)[Aliasable::class])) {
                $condition[] = 'alias=:id_alias';
            }
            if (isset(class_implements($class)[Slugable::class]) && !isset(class_implements($class)[Aliasable::class])) {
                $condition[] = 'slug=:id_alias';
            }
        }
        return $condition;
    }

    /**
     * @name $id_alias
     * @return Model
     */
    public function getModel($id_alias)
    {
        /** @var Model $model */
        $model = $this->get($id_alias)->getModel();
        if (!$model instanceof Model) throw new NotImplementedException(get_class($model));

        return $model;
    }

    /**
     * @return Model
     */
    public function getModelClass()
    {
        /** @var Model $class */
        $class = $this->modelName;
        if (!is_subclass_of($class, Model::class)) throw new NotImplementedException($class);

        return $class;
    }

    /**
     * @return mixed
     */
    protected function getObjectClass()
    {
        $class = $this->objectName;
        if (!is_subclass_of($class, ApiObject::class)) throw new NotImplementedException($class);

        return $class;
    }
}
