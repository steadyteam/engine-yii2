<?php

namespace Steady\Engine\Modules\User\Forms;

use Steady\Engine\Base\Form;
use Steady\Engine\Modules\User\Models\AuthModel;
use Steady\Engine\Modules\User\Models\ProfileModel;
use Steady\Engine\Modules\User\Models\UserModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\EscapeValidator;
use Steady\Engine\Validators\PasswordValidator;

class RegistrationForm extends Form
{
    public $name;
    public $username;
    public $password;
    public $passwordRepeat;

    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'required'],
            ['username', EscapeValidator::class],
            ['username', 'email'],
            ['username', 'required'],
            ['password', PasswordValidator::class],
            ['password', 'comparePassword'],
            ['password', 'required'],
            ['passwordRepeat', PasswordValidator::class],
            ['passwordRepeat', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'username' => 'Адрес электронной почты',
            'password' => 'Пароль',
            'passwordRepeat' => 'Повторите пароль',
        ];
    }

    /**
     * @throws \yii\db\Exception
     * @throws \Exception
     * @return UserModel
     * @throws \Throwable
     */
    public function registration()
    {
        $transaction = SW::$app->db->beginTransaction();

        try {
            $user = new UserModel();
            $user->username = $this->username;
            $user->password = $this->password;
            $user->registration();

            $profile = new ProfileModel();
            $profile->user_id = $user->user_id;
            $profile->name = $this->name;
            $profile->saveEx();

            $authModel = new AuthModel();
            $authModel->username = $this->username;
            $authModel->password = $this->password;
            $success = $authModel->login();

            if (!$success) {
                throw new \Exception('User Not Login');
            }

            $transaction->commit();
            return $user;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @name $attribute
     * @name $params
     * @return bool
     */
    protected function comparePassword($attribute, $params)
    {
        if ($this->password !== $this->passwordRepeat) {
            $this->addError($attribute, 'Пароли должны совпадать');

            return false;
        }

        return true;
    }
}