<?php

namespace Steady\Engine\Modules\User\Components;

use Steady\Engine\Modules\Page\Components\PageController;
use Steady\Engine\Modules\User\Forms\RegistrationForm;
use Steady\Engine\Modules\User\Models\AuthModel;
use Steady\Engine\Modules\User\Models\ProfileModel;
use Steady\Engine\SW;

class UserController extends PageController
{
    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionLogin()
    {
        $model = new AuthModel();

        if (!SW::$app->user->isGuest || ($model->load(SW::$app->request->post()) && $model->login())) {
            return $this->redirect(['/user/profile']);
        } else {
            return $this->render('user/login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    public function actionRegistration()
    {
        $form = new RegistrationForm();

        if (!SW::$app->user->isGuest || ($form->load(SW::$app->request->post()) && $form->registration())) {
            return $this->redirect(['/user/profile']);
        } else {
            return $this->render('user/registration', [
                'model' => $form,
            ]);
        }
    }

    public function actionProfile()
    {
        $model = new ProfileModel();
    }
}