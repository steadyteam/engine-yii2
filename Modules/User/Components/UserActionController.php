<?php

namespace Steady\Engine\Modules\User\Components;

use Steady\Engine\Base\Controller;
use Steady\Engine\Modules\User\Models\AuthModel;
use Steady\Engine\SW;

class UserActionController extends Controller
{
    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionLogin()
    {
        $model = new AuthModel();

        if (!SW::$app->user->isGuest || ($model->load(SW::$app->request->post()) && $model->login())) {
            return $this->redirect(SW::$app->user->getReturnUrl(['/user/profile']));
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        SW::$app->user->logout();

        return $this->redirect(SW::$app->homeUrl);
    }
}