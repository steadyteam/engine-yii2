<?php

namespace Steady\Engine\Modules\User\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Base\Model;
use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\EscapeValidator;
use yii\db\Expression;

/**
 * @property int auth_id
 * @property string username
 * @property string password
 * @property boolean success
 * @property string ip
 * @property string user_agent
 * @property string time
 */
class AuthModel extends Model
{
    const CACHE_KEY = 'sw_auth_tries';

    /**
     * @var boolean
     */
    public $remember;

    /**
     * @var bool|UserModel
     */
    private $_user = null;

    public static function tableName()
    {
        return 'users_auth';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'auth_id' => $migration->primaryKey(),
            'username' => $migration->string(32)->notNull(),
            'password' => $migration->string(128)->notNull(),
            'success' => $migration->boolean()->notNull()->defaultValue(false),
            'ip' => $migration->string(128)->notNull(),
            'user_agent' => $migration->string(256)->notNull(),
            'time' => $migration->dateTime()->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        return [
            ['username', EscapeValidator::class],
            ['username', 'required'],
            ['password', EscapeValidator::class],
            ['password', 'required'],
            ['password', 'validatePassword'],
            ['success', 'boolean'],
            ['success', 'default', 'value' => false],
            ['ip', 'ip'],
            ['ip', 'required'],
            ['user_agent', 'string'],
            ['user_agent', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => SW::t('admin', 'Username'),
            'password' => SW::t('admin', 'Password'),
            'remember' => SW::t('admin', 'Remember me'),
        ];
    }

    /**
     * @name $attribute
     * @name $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, SW::t('admin', 'Incorrect username or password.'));
            }
        }
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function login()
    {
        $cache = SW::$app->cache;

        if (($tries = (int)$cache->get(self::CACHE_KEY)) > 5) {
            $this->addError('username',
                SW::t('admin', 'You tried to login too often. Please wait 5 minutes.'));
            return false;
        }

        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $this->time = new Expression('NOW()');

        if ($this->validate()) {
            $this->success = 1;
        } else {
            $this->success = 0;
            $cache->set(self::CACHE_KEY, ++$tries, 300);
        }
        $this->password = '******';

        $this->insert(false);

        if ($this->success) {
            return SW::$app->user->login($this->getUser(), SettingModel::get('auth_time') ?: null);
        } else {
            return false;
        }

    }

    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = UserModel::findByUsername($this->username);
        }

        return $this->_user;
    }
}
