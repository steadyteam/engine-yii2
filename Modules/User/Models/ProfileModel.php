<?php

namespace Steady\Engine\Modules\User\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use yii\helpers\ArrayHelper;

/**
 * @property int profile_id
 * @property int user_id
 * @property int name
 */
class ProfileModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'users_profiles';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'profile_id' => $migration->primaryKey(),
            'user_id' => $migration->integer(11)->unique(),
            'name' => $migration->string(128),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['user_id', 'integer'],
            ['user_id', 'unique'],
            ['user_id', 'required'],
            ['name', 'string'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'username' => 'Имя',
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public function getUser()
    {
        $this->hasOne(UserModel::tableName(), ['user_id' => 'user_id']);
    }
}
