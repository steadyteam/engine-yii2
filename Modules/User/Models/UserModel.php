<?php

namespace Steady\Engine\Modules\User\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * @property int user_id
 * @property string username
 * @property string password
 * @property string auth_key
 * @property string access_token
 */
class UserModel extends AdvancedModel implements IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'user_id' => $migration->primaryKey(),
            'username' => $migration->string(32)->notNull()->unique(),
            'password' => $migration->string(128)->notNull(),
            'auth_key' => $migration->string(128),
            'access_token' => $migration->string(128),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['username', 'string', 'max' => 32],
            ['username', 'required'],
            ['username', 'unique'],
            ['password', 'safe'],
            ['password', 'required', 'on' => 'create'],
            ['access_token', 'default', 'value' => null],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(ProfileModel::class, ['user_id' => 'user_id']);
    }

    public function attributeLabels()
    {
        $array = [
            'username' => SW::t('admin', 'Username'),
            'password' => SW::t('admin', 'Password'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @name bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = $this->generateAuthKey();
                $this->password = $this->hashPassword($this->password);
            } else {
                $this->password = $this->password != '' ? $this->hashPassword($this->password)
                    : $this->oldAttributes['password'];
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @name int|string $id
     * @return null|UserModel
     */
    public static function findIdentity($id): ?UserModel
    {
        return static::findOne($id);
    }

    /**
     * @name mixed $token
     * @name null $type
     * @return null|UserModel
     */
    public static function findIdentityByAccessToken($token, $type = null): ?UserModel
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @name $username
     * @return null|UserModel
     */
    public static function findByUsername(string $username): ?UserModel
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey(): string
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @name $password
     * @return bool
     */
    public function validatePassword($password): bool
    {
        return $this->password === $this->hashPassword($password);
    }

    /**
     * @name $password
     * @return string
     */
    private function hashPassword($password): string
    {
        return sha1($password . $this->getAuthKey() . SettingModel::get('password_salt'));
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    private function generateAuthKey(): string
    {
        return SW::$app->security->generateRandomString();
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function registration()
    {
        return $this->saveEx();
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->user_id;
    }

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return $this->user_id;
    }
}
