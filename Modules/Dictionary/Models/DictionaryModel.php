<?php

namespace Steady\Engine\Modules\Dictionary\Models;

use paulzi\jsonBehavior\JsonValidator;
use Steady\Engine\Base\Migration;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int dictionary_id
 * @property string alias
 * @property boolean folder
 * @property string title
 * @property string value
 * @property array settings
 *
 * @mixin SluggableBehavior
 */
class DictionaryModel extends TreeModel implements Aliasable
{
    public function behaviors()
    {
        $array = [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'alias',
                'attribute' => 'text',
                'immutable' => true,
                'ensureUnique' => false,
            ],
            'json' => [
                'attributes' => ['settings'],
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->folder = false;
        }
    }

    public static function tableName()
    {
        return 'dictionaries';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'dictionary_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'folder' => $migration->boolean()->notNull()->defaultValue(false),
            'title' => $migration->string(256)->notNull(),
            'value' => $migration->string(256)->notNull(),
            'settings' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['folder', 'boolean'],
            ['folder', 'default', 'value' => false],
            ['title', 'required'],
            ['title', 'trim'],
            ['title', 'string', 'max' => 256],
            ['value', 'required'],
            ['value', 'trim'],
            ['value', 'string', 'max' => 256],
            ['settings', JsonValidator::class, 'merge' => true],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'alias' => SW::t('admin', 'Alias'),
            'folder' => SW::t('admin', 'Folder'),
            'title' => SW::t('admin', 'Title'),
            'value' => SW::t('admin', 'Value'),
            'settings' => SW::t('admin', 'Settings'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new DictionaryModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->folder = true;
        $model->title = 'Base';
        $model->value = 0;
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    /**
     * @param string $alias
     * @param string $title
     * @param string|int $value
     * @param int $parentId
     * @param bool $folder
     * @return DictionaryModel
     * @throws \yii\db\Exception
     */
    public static function create(string $alias, string $title, $value, int $parentId, bool $folder = false): DictionaryModel
    {
        $model = new DictionaryModel();
        $model->alias = $alias;
        $model->folder = $folder;
        $model->title = $title;
        $model->value = $value;
        $model->parent_id = $parentId;
        $model->saveEx();
        return $model;
    }
}