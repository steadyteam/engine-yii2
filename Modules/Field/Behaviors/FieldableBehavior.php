<?php

namespace Steady\Engine\Modules\Field\Behaviors;

use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Modules\Field\Components\Fieldable;
use Steady\Engine\Modules\Field\Models\FieldAssignModel;
use Steady\Engine\Modules\Field\Models\FieldModel;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property AdvancedModel owner
 * @property FieldModel[] fieldsRelation
 * @property array fields
 */
class FieldableBehavior extends Behavior implements Fieldable
{
    /**
     * @var array
     */
    private $_fields = [];

    /**
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_INIT => 'initModel',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function initModel()
    {
        //$this->getFieldsArray();
    }

    /**
     * @throws \yii\db\Exception
     */
    public function afterSave()
    {
        if (count($this->_fields)) {
            foreach ($this->_fields as $alias => $content) {
                $assign = $this->setField($alias, $content);
            }
        }
    }

    /**
     *
     */
    public function beforeDelete()
    {

    }

    /**
     * @return ActiveQuery
     */
    public function getFieldsRelation(): ActiveQuery
    {
        return $this->owner->hasMany(FieldModel::class, ['field_id' => 'field_id'])
            ->via('fieldsAssignsRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getFieldsAssignsRelation(): ActiveQuery
    {
        return $this->owner->hasMany(FieldAssignModel::class, ['owner_id' => $this->owner->primaryKey()[0]])
            ->where([FieldAssignModel::tableName() . '.class_alias' => $this->owner->getAlias()]);
    }

    /**
     * @return array
     */
    public function getFieldsForOwner()
    {
        if (!$this->owner->primaryKey) {
            return [];
        }
        return FieldAssignModel::getDataForOwner($this->owner->primaryKey, $this->owner->getAlias());
    }

    /**
     * @return array
     */
    public function getFieldsForClass()
    {
        return FieldAssignModel::getDataForClass($this->owner->getAlias());
    }

    /**
     * @name string $alias
     * @name int $type
     * @name bool $required
     * @name bool $folder
     * @return null|FieldModel
     * @throws \yii\db\Exception
     */
    public function addField(string $alias, int $type, bool $required = false, bool $folder = false): ?FieldModel
    {
        $field = FieldModel::findOne(['alias' => $alias, 'type' => $type]);
        if (!$field) {
            $field = FieldModel::create($alias, $type, $required, $folder);
        }
        $assign = FieldAssignModel::getAssign($field->field_id, $this->owner->primaryKey, $this->owner->getAlias());
        if (!$assign) {
            $assign = FieldAssignModel::create($field->field_id, $this->owner->primaryKey, $this->owner->getAlias());
        }

        if ($field->saveEx() && $assign->saveEx()) {
            return $field;
        }

        return null;
    }

    /**
     * @name string $alias
     * @name string $content
     * @return FieldAssignModel
     * @throws \yii\db\Exception
     */
    public function setField(string $alias, string $content): ?FieldAssignModel
    {
        $field = FieldModel::findOne(['alias' => $alias]);
        if (!$field) return null;

        $assign = FieldAssignModel::getAssign($field->field_id, $this->owner->primaryKey, $this->owner->getAlias());
        if (!$assign) {
            $assign = FieldAssignModel::create($field->field_id, $this->owner->primaryKey, $this->owner->getAlias());
        }

        $assign->content = $content;
        $assign->saveEx();

        return $assign;
    }

    /**
     * @name string $alias
     * @return bool|false|int
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteField(string $alias)
    {
        $field = FieldModel::findOne(['alias' => $alias]);
        if (!$field) return false;

        $assign = FieldAssignModel::getAssign($field->field_id, $this->owner->primaryKey, $this->owner->getAlias());
        if (!$assign) return false;

        return $assign->delete();
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        if (empty($this->_fields)) {
            $this->_fields = [];
            foreach ($this->owner->fieldsRelation as $field) {
                $this->_fields[$field->alias] = FieldAssignModel::getAssign($field->field_id, $this->owner->primaryKey,
                    $this->owner->getAlias())->content;
            }
        }
        return $this->_fields;
    }

    /**
     * @name array $fields
     * @return bool
     */
    public function setFields(array $fields): bool
    {
        foreach ($fields as $alias => $content) {
            $this->_fields[$alias] = $content;
        }

        return true;
    }

    /**
     * @name array $fields
     * @return bool
     */
    public function setFieldsArray(array $fields): bool
    {
        $this->_fields = $fields;
        return true;
    }
}