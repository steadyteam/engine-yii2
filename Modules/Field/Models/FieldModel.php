<?php

namespace Steady\Engine\Modules\Field\Models;

use paulzi\jsonBehavior\JsonValidator;
use Steady\Engine\Base\Migration;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int field_id
 * @property string alias
 * @property boolean folder
 * @property boolean required
 * @property string title
 * @property string content
 * @property int frequency
 * @property array settings
 *
 * @mixin SluggableBehavior
 */
class FieldModel extends TreeModel implements Aliasable
{
    const TYPE_STRING = 1;
    const TYPE_INTEGER = 2;
    const TYPE_FLOAT = 3;
    const TYPE_BOOLEAN = 5;
    const TYPE_ARRAY = 7;
    const TYPE_INTERVAL = 9;
    const TYPE_DICTIONARY = 11;
    const TYPE_TAGS = 15;

    /**
     * @var array
     */
    public $options;

    public function behaviors()
    {
        $array = [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'alias',
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
            'json' => [
                'attributes' => ['settings'],
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->folder = false;
            $this->required = false;
        }
    }

    public static function tableName()
    {
        return 'fields';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'field_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'folder' => $migration->boolean()->notNull()->defaultValue(false),
            'required' => $migration->boolean()->notNull()->defaultValue(false),
            'title' => $migration->string(256)->notNull(),
            'content' => $migration->text(),
            'frequency' => $migration->integer(11)->null(),
            'settings' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['folder', 'boolean'],
            ['folder', 'default', 'value' => false],
            ['required', 'boolean'],
            ['required', 'default', 'value' => false],
            ['title', 'required'],
            ['title', 'trim'],
            ['title', 'string', 'max' => 128],
            ['content', 'string'],
            ['content', 'trim'],
            ['frequency', 'integer'],
            ['settings', JsonValidator::class, 'merge' => true],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'alias' => SW::t('admin', 'Alias'),
            'required' => SW::t('admin', 'Required'),
            'title' => SW::t('admin', 'Title'),
            'content' => SW::t('admin', 'Content'),
            'settings' => SW::t('admin', 'Settings'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new FieldModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->folder = true;
        $model->required = false;
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    /**
     * @name string $alias
     * @name int $type
     * @name bool $required
     * @name bool $folder
     * @return FieldModel
     * @throws \yii\db\Exception
     */
    public static function create(string $alias, int $type, bool $required = false, bool $folder = false): FieldModel
    {
        $model = new FieldModel();
        $model->alias = $alias;
        $model->type = $type;
        $model->folder = $folder;
        $model->required = $required;
        $model->title = ucfirst($alias);
        $model->parent_id = 1;
        $model->saveEx();
        return $model;
    }

    /**
     * @return array
     */
    public function types(): array
    {
        $array = [
            self::TYPE_STRING => 'Строка',
            self::TYPE_INTEGER => 'Целое число',
            self::TYPE_FLOAT => 'Дробное число',
            self::TYPE_BOOLEAN => 'Логический',
            self::TYPE_ARRAY => 'Массив',
            self::TYPE_INTERVAL => 'Интервал',
            self::TYPE_DICTIONARY => 'Справочник',
            self::TYPE_TAGS => 'Тег',
        ];

        return ArrayHelper::merge($array, parent::types());
    }
}