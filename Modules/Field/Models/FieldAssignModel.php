<?php

namespace Steady\Engine\Modules\Field\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Base\Model;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\helpers\ArrayHelper;

/**
 * @property int field_assign_id
 * @property int field_id
 * @property int owner_id
 * @property string class_alias
 * @property string content
 */
class FieldAssignModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'fields_assign';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'field_assign_id' => $migration->primaryKey(),
            'field_id' => $migration->integer(11)->notNull(),
            'owner_id' => $migration->integer(11)->notNull(),
            'class_alias' => $migration->string(128)->notNull(),
            'content' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['field_id', 'integer'],
            ['field_id', 'required'],
            ['owner_id', 'integer'],
            ['owner_id', 'required'],
            ['class_alias', SlugAdvancedValidator::class],
            ['class_alias', 'required'],
            ['content', 'string'],
            ['content', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getFieldRelation()
    {
        return $this->hasOne(FieldModel::class, ['field_id' => 'field_id']);
    }

    /**
     * @name int $field_id
     * @name int $owner_id
     * @name string $class_alias
     * @return FieldAssignModel
     * @throws \yii\db\Exception
     */
    public static function create(int $field_id, int $owner_id, string $class_alias): FieldAssignModel
    {
        $model = new FieldAssignModel();
        $model->field_id = $field_id;
        $model->owner_id = $owner_id;
        $model->class_alias = $class_alias;
        $model->saveEx();
        return $model;
    }

    /**
     * @name FieldModel $fieldModel
     * @name Model $ownerModel
     * @return bool
     * @throws \Exception
     */
    public function appendAndSave($fieldModel, $ownerModel)
    {
        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $saved = $fieldModel->save();
            if (!$saved) {
                throw new \Exception('FieldModel not saved: ' . $fieldModel->formatErrors());
            }

            $this->field_id = $fieldModel->primaryKey;
            $this->owner_id = $ownerModel->primaryKey;
            $this->class_alias = $ownerModel->getAlias();

            $saved = $this->save();
            if (!$saved) {
                throw new \Exception(get_class($this) . ' not saved: ' . $this->formatErrors());
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * @name int $field_id
     * @name int $owner_id
     * @name string $class_alias
     * @return null|FieldAssignModel
     */
    public static function getAssign(int $field_id, int $owner_id, string $class_alias): ?FieldAssignModel
    {
        return FieldAssignModel::findOne(['field_id' => $field_id, 'owner_id' => $owner_id,
            'class_alias' => $class_alias]);
    }

    /**
     * @name int $owner_id
     * @name string $class_alias
     * @return array
     */
    public static function getDataForOwner(int $owner_id, string $class_alias): array
    {
        $data = self::find()
            ->joinWith('fieldRelation')
            ->where([self::tableName() . '.owner_id' => $owner_id, self::tableName() . '.class_alias' => $class_alias])
            ->asArray()
            ->all();

        return $data;
    }

    /**
     * @name string $class_alias
     * @return array
     */
    public static function getDataForClass(string $class_alias): array
    {
        return self::find()
            ->select([FieldModel::tableName() . '.*'])
            ->leftJoin(FieldModel::tableName(), FieldModel::tableName() . '.field_id = '
                . FieldAssignModel::tableName() . '.field_id')
            ->where(['class_alias' => $class_alias])
            ->groupBy([FieldAssignModel::tableName() . '.field_id'])
            ->asArray()
            ->all();
    }
}