<?php

namespace Steady\Engine\Modules\Field\Components;

use Steady\Engine\Modules\Field\Models\FieldModel;

interface Fieldable
{
    public function addField(string $alias, int $type, bool $required = false, bool $folder = false): ?FieldModel;

    public function setField(string $alias, string $content);

    public function deleteField(string $alias);

}