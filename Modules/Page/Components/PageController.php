<?php

namespace Steady\Engine\Modules\Page\Components;

use Steady\Admin\Modules\Page\PageModule;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Base\Controller;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Engine\Modules\Layout\Api\LayoutObject;
use Steady\Engine\Modules\Layout\Models\LayoutModel;
use Steady\Engine\Modules\Page\Api\PageObject;
use Steady\Engine\SW;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * @var PageObject
     */
    public $pageObject;

    /**
     * @var LayoutObject
     */
    public $layoutObject;

    /**
     * @var string
     */
    public $viewExtension = 'twig';

    /**
     * @var string
     */
    protected $pageSlug;

    /**
     * Set the page and set the layout from the pages db
     *
     * @name \yii\base\Action $action
     * @return bool
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function beforeAction($action): bool
    {
        $this->pageSlug = $this->getPageSlug();

        $this->pageObject = SW::$app->api->page->get($this->pageSlug);
        if (!$this->pageObject || !$this->pageObject->page_id) {
            throw new NotFoundHttpException("Page `$this->pageSlug` not found");
        }
        $this->layoutObject = SW::$app->api->layout->get($this->pageObject->layout_id);
        if (!$this->layoutObject || !$this->layoutObject->layout_id) {
            throw new NotFoundHttpException("Layout `" . $this->pageObject->layout_id . "` not found");
        }

        $this->layout = $this->layoutObject->alias;

        $this->makeSeo($this->pageObject);
        $this->makeOpenGraph($this->pageObject);

        return parent::beforeAction($action);
    }

    /**
     * Set the view path to 'pages' or 'pages/slug'
     * @return string
     * @throws InvalidArgumentException
     * @throws \Exception
     */
    public function getViewPath()
    {
        $viewPath = SW::getAlias("@app/Resources/views/pages");
        $runtimeView = SW::getAlias(PageModule::$moduleConfig['runtimeViewPath']);

        if (file_exists("$viewPath/$this->pageSlug.$this->viewExtension")) {
            return $viewPath;
        } else if (file_exists("$viewPath/$this->pageSlug/$this->pageSlug.$this->viewExtension")) {
            $viewPath .= "/$this->pageSlug";
        } else if (file_exists("$runtimeView/$this->pageSlug.$this->viewExtension")) {
            $viewPath = $runtimeView;
        } else {
            $this->pageObject->createViewFile($this->viewExtension);
            $viewPath = $runtimeView;
        }

        return $viewPath;
    }

    /**
     * Render the view file or the page content in layout
     * @name string $view
     * @name array $params
     * @return string
     * @throws InvalidArgumentException
     */
    public function render($view, $params = [])
    {
        $params['page'] = $this->pageObject;
        if ($this->layoutObject->type == LayoutModel::TYPE_CONTENT_RENDER) {
            return $this->renderContent("");
        }
        return parent::render("$view.$this->viewExtension", $params);
    }

    /**
     * Show page
     * @name $slug
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionView($slug)
    {
        return $this->render($this->pageSlug, [

        ]);
    }

    /**
     * Устанавливает SEO тэги для страницы. Берет данные из параметра $object,
     * если отправлен параметр $page, то добавляет и его параметры
     *
     * @param ApiObject $object
     * @param PageObject|null $page
     * @param string|null $title
     * @param string|null $description
     * @param string|null $keywords
     * @param array $params
     */
    protected function makeSeo($object, $page = null, $title = null, $description = null, $keywords = null, $params = [])
    {
        $title = $title ?? ($object->seo('title', $object->title ?? ''));
        $description = $description ?? ($object->seo('description'));
        $keywords = $keywords ?? ($object->seo('keywords'));
        $canonical = ($object->seo('canonical'));

        // Seo Templates
        if ($page) {
            $pageTitle = $page->seo('title');
            $pageDesc = $page->seo('description');
            $pageKeywords = $object->seo('keywords');

            $tp = $params['titlePattern'] ?? '{title} | {pageTitle}';
            $dp = $params['descriptionPattern'] ?? '{description}. {pageDesc}';

            // TODO: Временное решение, стоит улучшить в дальнейшем
            if ($title === 'Base') {
                $tp = '{pageTitle}';
            }

            $title = $pageTitle ? str_replace(['{title}', '{pageTitle}'], [$title, $pageTitle], $tp) : $title;
            $description = $pageDesc ? str_replace(['{description}', '{pageDesc}'], [$description, $pageDesc], $dp)
                : $description;
            $keywords .= $pageKeywords ? ", $pageKeywords" : '';
        }

        $this->view->title = $title;
        if ($description) {
            $this->view->registerMetaTag(['name' => 'description', 'content' => $description]);
        }
        if ($keywords) {
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => $keywords]);
        }
        if ($canonical) {
            $this->view->registerLinkTag(['rel' => 'canonical', 'href' => $canonical]);
        }
    }

    /**
     * Устанавливает OpenGraph теги для страницы
     *
     * @param ApiObject $object
     * @param string|null $image
     * @param string|null $siteName
     * @param string $locale
     * @param string $type
     */
    protected function makeOpenGraph($object, $image = null, $siteName = null, $locale = 'ru_RU', $type = 'website')
    {
        $image = $image ?? Url::to('@url-static/img/style/og-base-1.png', true);
        $siteName = $siteName ?? SW::$app->request->serverName;

        $this->view->registerMetaTag(['name' => 'og:site_name', 'content' => $siteName]);
        $this->view->registerMetaTag(['name' => 'og:locale', 'content' => $locale]);
        $this->view->registerMetaTag(['name' => 'og:type', 'content' => $type]);
        $this->view->registerMetaTag(['name' => 'og:url', 'content' => SW::$app->request->absoluteUrl]);
        $this->view->registerMetaTag(['name' => 'og:title', 'content' => $this->view->title]);
        $this->view->registerMetaTag(['name' => 'og:description', 'content' => $object->seo('description')]);
        $this->view->registerMetaTag(['name' => 'og:image', 'content' => $image]);
        $this->view->registerMetaTag(['name' => 'og:image:width', 'content' => '600']);
        $this->view->registerMetaTag(['name' => 'og:image:height', 'content' => '315']);
    }

    /**
     * @return string
     */
    protected function getPageSlug()
    {
        $queryParams = SW::$app->request->queryParams;
        if (isset($queryParams['slug'])) {
            return $queryParams['slug'];
        } else {
            return 'index';
        }
    }

    /**
     * @name ApiObject|TreeObjectTrait $object
     * @name bool $insertSelf
     * @return array
     */
    protected function getBreadcrumbs(ApiObject $object, $insertSelf = false)
    {
        $breadcrumbs = [];
        if (!$object || !$object instanceof TreeObjectInterface) return [];
        $parents = $object->parents();
        if ($insertSelf) {
            $parents[] = $object;
        }
        foreach ($parents as $parent) {
            if ($parent->depth >= 1) {
                $url = method_exists($parent, 'url') ? $parent->url() : Url::to($parent->slug);
                $breadcrumbs[] = ['label' => $parent->title, 'url' => $url];
            }
        }
        return $breadcrumbs;
    }
}