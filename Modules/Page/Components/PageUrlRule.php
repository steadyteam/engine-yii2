<?php

namespace Steady\Engine\Modules\Page\Components;

use Steady\Engine\Modules\Layout\Models\LayoutModel;
use Steady\Engine\Modules\Page\Models\PageModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use Steady\Modules\Seo\Models\RedirectModel;
use yii\base\BaseObject;
use yii\web\UrlRuleInterface;

class PageUrlRule extends BaseObject implements UrlRuleInterface
{
    /**
     * @name \yii\web\UrlManager $manager
     * @name string $route
     * @name array $params
     * @return array|bool|string
     */
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'page/view' && isset($params['slug'])) {
            return '/' . $params['slug'];
        }
        return false;
    }

    /**
     * @name \yii\web\UrlManager $manager
     * @name \yii\web\Request $request
     * @return array|bool
     * @throws \yii\base\InvalidConfigException
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if (!$pathInfo) {
            $pathInfo = 'index';
        }

        if (preg_match(SlugAdvancedValidator::SLUG_ADVANCED_PATTERN, $pathInfo)) {
            /** @var PageModel $page */
            $page = PageModel::find()->where(['slug' => $pathInfo])->andWhere(['!=', 'type', 0])->one();

            $getRoute = function (string $route, ?string $slug) {
                if (count(explode('/', $route)) === 1) {
                    return ["page/$route", ['slug' => $slug]];
                } else {
                    return [$route, ['slug' => $slug]];
                }
            };

            if ($page) {
                if ($page->route) {
                    return $getRoute($page->route, $page->slug);
                } else if ($page->layoutRelation->route) {
                    return $getRoute($page->layoutRelation->route, $page->slug);
                } else {
                    return ['page/view', ['slug' => $page->slug]];
                }
            }

            // TODO: Проверка на подключение модуля, перенести в другое место.
            $redirect = RedirectModel::find()->where(['from' => SW::$app->request->pathInfo])->one();
            if ($redirect) {
                SW::$app->response->redirect($redirect->to, $redirect->status);
                return false;
            }
        }

        return false;
    }
}