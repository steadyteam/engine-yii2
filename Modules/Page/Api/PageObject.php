<?php

namespace Steady\Engine\Modules\Page\Api;

use Steady\Admin\Modules\Page\PageModule;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Engine\Modules\Page\Models\PageModel;
use Steady\Engine\SW;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * @property PageModel model
 */
class PageObject extends ApiObject implements TreeObjectInterface
{
    use TreeObjectTrait;

    public $modelName = PageModel::class;

    public $page_id;
    public $layout_id;
    public $alias;
    public $slug;
    public $title;
    public $image;
    public $content;
    public $preview;
    public $created_at;

    /**
     * @name bool $scheme
     * @return string
     */
    public function url($scheme = false)
    {
        return Url::to(['/page/view', 'slug' => $this->slug], $scheme);
    }

    /**
     * @name $viewExtension
     * @return bool
     * @throws \Exception
     */
    public function createViewFile($viewExtension)
    {
        $runtimeView = SW::getAlias(PageModule::$moduleConfig['runtimeViewPath']);
        $filePath = "$runtimeView/$this->slug.$viewExtension";

        $parentSlug = explode('/', $this->slug)[0];
        if ($parentSlug) {
            $runtimeView .= "/$parentSlug";
        }

        try {
            FileHelper::createDirectory($runtimeView);
            file_put_contents($filePath, $this->content);
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }
}