<?php

namespace Steady\Engine\Modules\Page\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Engine\Modules\Page\Models\PageModel;

class PageApi extends Api
{
    use TreeApiTrait;

    public $modelName = PageModel::class;

    public $objectName = PageObject::class;
}