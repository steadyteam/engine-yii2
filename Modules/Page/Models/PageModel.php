<?php

namespace Steady\Engine\Modules\Page\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\Slugable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Modules\Layout\Models\LayoutModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int page_id
 * @property string alias
 * @property int layout_id
 * @property string slug
 * @property string route
 * @property string title
 * @property string image
 * @property string preview
 * @property string content
 *
 * @property LayoutModel layoutRelation
 *
 * @mixin SeoBehavior
 * @mixin SluggableBehavior
 */
class PageModel extends TreeModel implements Aliasable, Slugable
{
    const TYPE_DEFAULT = 1;

    public function behaviors()
    {
        $array = [
            'seo' => SeoBehavior::class,
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'slug',
                'attribute' => 'alias',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->layout_id = 1;
        }
    }

    public static function tableName()
    {
        return 'pages';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'page_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'layout_id' => $migration->integer(11)->notNull()->defaultValue(1),
            'slug' => $migration->string(256)->notNull()->unique(),
            'route' => $migration->string(128)->null(),
            'title' => $migration->string(256)->notNull(),
            'image' => $migration->string(512)->null(),
            'preview' => $migration->text(),
            'content' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        //$migration->createIndexNamed(static::tableName(), 'alias', true);
        //$migration->createIndexNamed(static::tableName(), 'slug',true);
        //$migration->addForeignKeyNamed(static::tableName(), 'layout_id', LayoutModel::tableName(), 'layout_id');

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugAdvancedValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['layout_id', 'integer'],
            ['layout_id', 'required'],
            ['layout_id', 'default', 'value' => 1],
            ['slug', SlugAdvancedValidator::class],
            //['slug', 'required'],
            ['slug', 'unique'],
            ['route', SlugAdvancedValidator::class],
            ['title', 'string', 'max' => 256],
            ['title', 'required'],
            ['title', 'trim'],
            ['image', 'image'],
            ['preview', 'trim'],
            ['content', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'parent_id' => SW::t('admin/page', 'Parent page'),
            'alias' => SW::t('admin', 'Alias'),
            'type' => SW::t('admin', 'Type'),
            'layout_id' => SW::t('admin', 'Layout'),
            'slug' => SW::t('admin', 'Slug'),
            'route' => SW::t('admin', 'Route'),
            'title' => SW::t('admin', 'Title'),
            'image' => SW::t('admin', 'Image'),
            'preview' => SW::t('admin', 'Preview'),
            'content' => SW::t('admin', 'Content'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLayoutRelation()
    {
        return $this->hasOne(LayoutModel::class, ['layout_id' => 'layout_id']);
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new PageModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->layout_id = 1;
        $model->slug = 'base';
        $model->title = 'Base';
        $model->parent_id = null;
        $model->hidden = true;
        $model->saveEx();
        return $model;
    }

    /**
     * @return array
     */
    public function types(): array
    {
        $array = [
            self::TYPE_DEFAULT => 'Стандартный'
        ];

        return ArrayHelper::merge($array, parent::types());
    }
}