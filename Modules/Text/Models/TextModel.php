<?php

namespace Steady\Engine\Modules\Text\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\CacheFlushBehavior;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugValidator;
use yii\helpers\ArrayHelper;

/**
 * @property int text_id
 * @property string alias
 * @property boolean folder
 * @property string text
 *
 * @mixin CacheFlushBehavior
 */
class TextModel extends TreeModel implements Aliasable
{
    const CACHE_KEY = 'sw_text';

    public function behaviors()
    {
        $array = [
            'cache' => CacheFlushBehavior::class,
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->folder = false;
        }
    }

    public static function tableName()
    {
        return 'texts';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'text_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'folder' => $migration->boolean()->notNull()->defaultValue(false),
            'text' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['folder', 'boolean'],
            ['folder', 'default', 'value' => false],
            ['text', 'required'],
            ['text', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'alias' => SW::t('admin', 'Alias'),
            'text' => SW::t('admin', 'Text'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new TextModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->folder = true;
        $model->text = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }
}