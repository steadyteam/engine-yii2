<?php

namespace Steady\Engine\Modules\Text\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Helpers\CacheHelper;
use Steady\Engine\Modules\Text\Models\TextModel;

class TextApi extends Api
{
    /**
     * @var array
     */
    private $_texts = [];

    /**
     * @throws \ReflectionException
     */
    public function init()
    {
        parent::init();

        $this->_texts = CacheHelper::cache(TextModel::CACHE_KEY, 3600, function () {
            return TextModel::find()->asArray()->all();
        });
    }

    public function get($id_alias)
    {
        if (($text = $this->findText($id_alias)) === null) {
            return $this->notFound($id_alias);
        }
        return $text['text'];
    }

    protected function findText($id_alias)
    {
        foreach ($this->_texts as $item) {
            if ($item['text_id'] == $id_alias || $item['alias'] == $id_alias) {
                return $item;
            }
        }
        return null;
    }

    protected function notFound($id_alias)
    {
        $text = 'Text not found';

        return $text;
    }
}