<?php

namespace Steady\Engine\Modules\Setting\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\CacheFlushBehavior;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Helpers\CacheHelper;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * @property integer setting_id
 * @property string alias
 * @property boolean folder
 * @property string value
 * @property string title
 *
 * @mixin CacheFlushBehavior
 */
class SettingModel extends TreeModel implements Aliasable
{
    const VISIBLE_NONE = 0;
    const VISIBLE_ROOT = 1;
    const VISIBLE_ALL = 2;

    const CACHE_KEY = 'sw_settings';

    /**
     * @var array
     */
    public static $_data;

    public function behaviors()
    {
        $array = [
            'cache' => CacheFlushBehavior::class,
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->folder = false;
        }
    }

    public static function tableName()
    {
        return 'settings';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'setting_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'folder' => $migration->boolean()->notNull()->defaultValue(false),
            'value' => $migration->string()->null(),
            'title' => $migration->string(256)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugAdvancedValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['folder', 'boolean'],
            ['folder', 'default', 'value' => false],
            ['value', 'safe'],
            ['value', 'trim'],
            ['title', 'string', 'max' => 256],
            ['title', 'required'],
            ['title', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'alias' => SW::t('admin', 'Alias'),
            'value' => SW::t('admin', 'Value'),
            'title' => SW::t('admin', 'Title'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return TreeModel
     * @throws Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new SettingModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->folder = true;
        $model->value = 'base';
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    /**
     * @name $alias
     * @return null|string
     */
    public static function get($alias): ?string
    {
        if (!self::$_data) {
            self::$_data = CacheHelper::cache(self::CACHE_KEY, 3600, function () {
                $result = [];
                try {
                    /** @var SettingModel[] $settings */
                    $settings = self::find()->all();
                    foreach ($settings as $setting) {
                        $result[$setting->alias] = $setting->value;
                    }
                } catch (\Exception $e) {
                    throw $e;
                }
                return $result;
            });
        }
        if (isset(self::$_data[$alias])) {
            $value = self::$_data[$alias];
        } else {
            /** @var SettingModel $setting */
            $setting = SettingModel::find()->where(['alias' => $alias])->one();
            $value = $setting->value ?? null;
        }
        return $value;
    }

    /**
     * @name $alias
     * @name $value
     * @name $title
     * @name int $access_level
     * @return SettingModel
     */
    public static function set($alias, $value, $title = false, $access_level = self::VISIBLE_ALL): SettingModel
    {
        /** @var SettingModel $setting */
        $setting = SettingModel::find()->where(['alias' => $alias])->one();
        if ($setting) {
            $setting->value = $value;
        } else {
            $setting = new SettingModel();
            $setting->alias = $alias;
            $setting->value = $value;
            $setting->title = $title ?? $alias;
            $setting->access_level = $access_level;
        }
        $setting->save();

        return $setting;
    }

    /**
     * @name $alias
     * @name $value
     * @name $title
     * @name int $access_level
     * @return SettingModel
     * @throws Exception
     */
    public static function setEx($alias, $value, $title, $access_level = self::VISIBLE_ALL): SettingModel
    {
        $setting = self::set($alias, $value, $title, $access_level);
        if (!$setting->save()) {
            throw new Exception('Setting not saved!', $setting->getErrors());
        }

        return $setting;
    }
}