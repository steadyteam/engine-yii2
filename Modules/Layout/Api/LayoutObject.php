<?php

namespace Steady\Engine\Modules\Layout\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Engine\Modules\Layout\Models\LayoutModel;

/**
 * @property LayoutModel $model
 */
class LayoutObject extends ApiObject implements TreeObjectInterface
{
    use TreeObjectTrait;

    public $modelName = LayoutModel::class;

    public $layout_id;
    public $alias;
    public $type;
    public $slug;
    public $title;
    public $content;
}