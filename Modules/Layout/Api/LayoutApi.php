<?php

namespace Steady\Engine\Modules\Layout\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Engine\Modules\Layout\Models\LayoutModel;

class LayoutApi extends Api
{
    use TreeApiTrait;

    public $modelName = LayoutModel::class;

    public $objectName = LayoutObject::class;
}