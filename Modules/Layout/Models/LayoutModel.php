<?php

namespace Steady\Engine\Modules\Layout\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use Steady\Engine\Validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int layout_id
 * @property string alias
 * @property string route
 * @property string title
 * @property string content
 *
 * @mixin SeoBehavior
 * @mixin SluggableBehavior
 */
class LayoutModel extends TreeModel implements Aliasable
{
    const TYPE_DEFAULT_RENDER = 1;
    const TYPE_CONTENT_RENDER = 2;

    public function behaviors()
    {
        $array = [
            //'seo' => SeoBehavior::class,
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'alias',
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();
    }

    public static function tableName()
    {
        return 'layouts';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'layout_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'route' => $migration->string(128)->null(),
            'title' => $migration->string(128)->notNull(),
            'content' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        //$migration->createIndexNamed(static::tableName(), 'alias', true);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['route', SlugAdvancedValidator::class],
            ['title', 'string', 'max' => 128],
            ['title', 'required'],
            ['title', 'trim'],
            ['content', 'string'],
            ['content', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'parent_id' => SW::t('admin/layout', 'Parent layout'),
            'alias' => SW::t('admin', 'Alias'),
            'route' => SW::t('admin', 'Route'),
            'title' => SW::t('admin', 'Name'),
            'content' => SW::t('admin', 'Content'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new LayoutModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    public function types(): array
    {
        $array = [
            self::TYPE_DEFAULT_RENDER => 'Стандартный',
            self::TYPE_CONTENT_RENDER => 'Контент',
        ];

        return ArrayHelper::merge($array, parent::types());
    }
}