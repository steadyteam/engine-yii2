<?php

namespace Steady\Engine\Modules\Module\Models;

use Steady\Admin\Components\SteadyModule;
use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\CacheFlushBehavior;
use Steady\Engine\Behaviors\SortableModelBehavior;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Helpers\CacheHelper;
use Steady\Engine\SW;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * @property int module_id
 * @property string name
 * @property string class
 * @property string title
 * @property string icon
 * @property array|string settings
 * @property int order_num
 *
 * @mixin CacheFlushBehavior
 * @mixin SortableModelBehavior
 */
class ModuleModel extends AdvancedModel
{
    const CACHE_KEY = 'sw_modules';

    public function behaviors()
    {
        $array = [
            'cache' => CacheFlushBehavior::class,
            'sortable' => SortableModelBehavior::class,
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();
    }

    public static function tableName()
    {
        return 'modules';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'module_id' => $migration->primaryKey(),
            'name' => $migration->string(128)->notNull()->unique(),
            'class' => $migration->string(128)->notNull()->unique(),
            'title' => $migration->string(128)->notNull(),
            'icon' => $migration->string(256),
            'settings' => $migration->text(),
            'order_num' => $migration->integer(3)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['name', 'match', 'pattern' => '/^[A-z]+$/'],
            ['name', 'required'],
            ['name', 'unique'],
            ['class', 'match', 'pattern' => '/^[\w\\\]+$/'],
            ['class', 'required'],
            ['class', 'checkExists'],
            ['title', 'required'],
            ['icon', 'string'],
            ['settings', 'safe'],
            ['order_num', 'integer'],
            [['name', 'class', 'title', 'icon'], 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'name' => SW::t('admin', 'Name'),
            'class' => SW::t('admin', 'Class'),
            'title' => SW::t('admin', 'Title'),
            'icon' => SW::t('admin', 'Icon'),
            'settings' => SW::t('admin', 'Settings'),
            'order_num' => SW::t('admin', 'Order'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @name array $moduleConfig
     * @return ModuleModel
     * @throws InvalidConfigException
     * @throws \yii\db\Exception
     */
    public static function install(array $moduleConfig): ModuleModel
    {
        $model = new ModuleModel();
        $model->name = $moduleConfig['name'];
        $model->class = $moduleConfig['class'];
        $model->title = $moduleConfig['title']['en'];
        $model->icon = $moduleConfig['icon'];
        $model->settings = SW::createObject($model->class, [$moduleConfig['name']])->settings;
        $model->order_num = $moduleConfig['order_num'];
        $model->status = ModuleModel::STATUS_ON;
        $model->saveEx();
        return $model;
    }

    /**
     * @name $id_name
     * @return false|int
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function uninstall($id_name)
    {
        $module = self::find()
            ->where(['or', 'module_id=:id_name', 'name=:id_name'], [':id_name' => $id_name])
            ->one();

        return $module->delete();
    }

    /**
     * @name bool $insert
     * @return bool
     * @throws InvalidConfigException
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!$this->settings || !is_array($this->settings)) {
                $this->settings = $this->getDefaultSettings();
            }
            $this->settings = json_encode($this->settings);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @throws InvalidConfigException
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->settings = $this->settings !== ''
            ? json_decode($this->settings, true)
            : $this->getDefaultSettings();
    }

    public static function findAllActive()
    {
        return CacheHelper::cache(self::CACHE_KEY, 3600, function () {
            $result = [];
            $modules = self::find()
                ->where(['status' => self::STATUS_ON])
                ->orderBy(['order_num' => SORT_DESC])
                ->all();

            /** @var ModuleModel $module */
            foreach ($modules as $module) {
                $module->trigger(self::EVENT_AFTER_FIND);
                $result[$module->name] = (object)$module->attributes;
            }

            return $result;
        });
    }

    public function setSettings($settings)
    {
        $newSettings = [];
        foreach ($this->settings as $key => $value) {
            $newSettings[$key] = is_bool($value) ? ($settings[$key] ? true : false)
                : ($settings[$key] ? $settings[$key] : '');
        }
        $this->settings = $newSettings;
    }

    public function checkExists($attribute)
    {
        if (!class_exists($this->$attribute)) {
            $this->addError($attribute, SW::t('admin', 'Class does not exist'));
        }
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getDefaultSettings()
    {
        /** @var SteadyModule $module */
        $module = SW::createObject($this->class, [$this->name]);
        if (isset($module->settings)) {
            return $module->settings;
        } else {
            return [];
        }
    }

}