<?php

namespace Steady\Engine\Modules\Block\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Engine\Modules\Block\Models\BlockModel;

class BlockApi extends Api
{
    use TreeApiTrait;

    public $modelName = BlockModel::class;

    public $objectName = BlockObject::class;
}