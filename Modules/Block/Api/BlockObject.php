<?php

namespace Steady\Engine\Modules\Block\Api;

use Steady\Admin\Modules\Page\PageModule;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Engine\Modules\Block\Models\BlockModel;
use Steady\Engine\SW;
use yii\helpers\FileHelper;

/**
 * @property BlockModel model
 */
class BlockObject extends ApiObject implements TreeObjectInterface
{
    use TreeObjectTrait;

    public $modelName = BlockModel::class;

    public $block_id;
    public $alias;
    public $layout_id;
    public $slug;
    public $title;
    public $image;
    public $content;
    public $preview;

    /**
     * @return bool
     * @throws \Exception
     */
    public function createViewFile()
    {
        $runtimeView = SW::getAlias(PageModule::$moduleConfig['runtimeViewPath']);
        $filePath = "$runtimeView/$this->slug.php";

        $parentSlug = explode('/', $this->slug)[0];
        if ($parentSlug) {
            $runtimeView .= "/$parentSlug";
        }

        try {
            FileHelper::createDirectory($runtimeView);
            file_put_contents($filePath, $this->content);
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }
}