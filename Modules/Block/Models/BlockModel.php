<?php

namespace Steady\Engine\Modules\Block\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\Slugable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Modules\Field\Behaviors\FieldableBehavior;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int block_id
 * @property string alias
 * @property int layout_id
 * @property string slug
 * @property string title
 * @property string image
 * @property string preview
 * @property string content
 *
 * @mixin FieldableBehavior
 * @mixin SeoBehavior
 * @mixin SluggableBehavior
 */
class BlockModel extends TreeModel implements Aliasable, Slugable
{
    const TYPE_DEFAULT = 1;
    const TYPE_NAVIGATION = 2;
    const TYPE_NAVIGATION_ITEM = 3;

    public function behaviors()
    {
        $array = [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'slug',
                'attribute' => 'alias',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->layout_id = 1;
        }
    }

    public static function tableName()
    {
        return 'blocks';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'block_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'layout_id' => $migration->integer(11)->notNull()->defaultValue(1),
            'slug' => $migration->string(128)->notNull(),
            'title' => $migration->string(256)->notNull(),
            'image' => $migration->string(512)->null(),
            'preview' => $migration->text(),
            'content' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['layout_id', 'required'],
            ['layout_id', 'integer'],
            ['layout_id', 'default', 'value' => 1],
            ['slug', 'string', 'max' => 512],
            // ['slug', SlugAdvancedValidator::class],
            //['slug', 'unique'],
            ['title', 'string', 'max' => 128],
            ['title', 'required'],
            ['title', 'trim'],
            ['image', 'image'],
            ['preview', 'string'],
            ['preview', 'trim'],
            ['content', 'string'],
            ['content', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'parent_id' => SW::t('admin/block', 'Parent block'),
            'alias' => SW::t('admin', 'Alias'),
            'layout_id' => SW::t('admin', 'Layout'),
            'slug' => SW::t('admin', 'Slug'),
            'title' => SW::t('admin', 'Title'),
            'image' => SW::t('admin', 'Image'),
            'preview' => SW::t('admin', 'Preview'),
            'content' => SW::t('admin', 'Content'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public function types(): array
    {
        $array = [
            self::TYPE_DEFAULT => 'Default',
            self::TYPE_NAVIGATION => 'Navigation',
            self::TYPE_NAVIGATION_ITEM => 'Navigation Item',
        ];

        return ArrayHelper::merge($array, parent::types());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new BlockModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->layout_id = 1;
        $model->slug = 'base';
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }
}