<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

defined('STD_PATH_ADMIN') or define('STD_PATH_ADMIN', STD_PATH_ROOT . '/admin');
defined('STD_PATH_BACKEND') or define('STD_PATH_BACKEND', STD_PATH_ROOT . '/backend');
defined('STD_PATH_ENGINE') or define('STD_PATH_ENGINE', STD_PATH_ROOT . '/engine');
defined('STD_PATH_FRONTEND') or define('STD_PATH_FRONTEND', STD_PATH_ROOT . '/frontend');
defined('STD_PATH_INSTALL') or define('STD_PATH_INSTALL', STD_PATH_ROOT . '/install');
defined('STD_PATH_MODULES') or define('STD_PATH_MODULES', STD_PATH_ROOT . '/modules');
defined('STD_PATH_PUBLIC') or define('STD_PATH_PUBLIC', STD_PATH_ROOT . '/public');
defined('STD_PATH_RUNTIME') or define('STD_PATH_RUNTIME', STD_PATH_ROOT . '/runtime');
defined('STD_PATH_VENDOR') or define('STD_PATH_VENDOR', STD_PATH_ROOT . '/vendor');

defined('STD_PATH_CONFIG_PERSONAL') or define('STD_PATH_CONFIG_PERSONAL', STD_PATH_ROOT . '/backend/Config/personal');

defined('STD_URL_PUBLIC') or define('STD_URL_PUBLIC', '/public');
defined('STD_URL_ASSETS') or define('STD_URL_ASSETS', STD_URL_PUBLIC . '/assets');
defined('STD_URL_DIST') or define('STD_URL_DIST', STD_URL_PUBLIC . '/dist');
defined('STD_URL_STATIC') or define('STD_URL_STATIC', STD_URL_PUBLIC . '/static');
defined('STD_URL_UPLOADS') or define('STD_URL_UPLOADS', STD_URL_PUBLIC . '/uploads');

defined('INSTALLED') or define('INSTALLED', true);

defined('LIVE_EDIT') or define('LIVE_EDIT', false);

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

require(STD_PATH_ROOT . '/vendor/autoload.php');
require(STD_PATH_ROOT . '/vendor/yiisoft/yii2/Yii.php');

if (STD_APP_TYPE === 'web') {
    if (INSTALLED) {
        $config = require(STD_PATH_BACKEND . '/Config/web.php');
    } else {
        $config = require(STD_PATH_INSTALL . '/Config/install-web.php');
    }
    /** @noinspection PhpUnhandledExceptionInspection */
    (new \Steady\Engine\Base\Application($config))->run();
} else if (STD_APP_TYPE === 'console') {
    $config = require(STD_PATH_BACKEND . '/Config/console.php');
    /** @noinspection PhpUnhandledExceptionInspection */
    $application = new yii\console\Application($config);
    $exitCode = $application->run();
    exit($exitCode);
}