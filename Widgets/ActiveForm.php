<?php

namespace Steady\Engine\Widgets;

use Steady\Engine\Base\Model;

class ActiveForm extends \yii\widgets\ActiveForm
{
    public function fieldEx(Model $model, string $attribute, array $options = [])
    {
        return $this->field($model, $attribute, $options)
            ->textInput(['placeholder' => $model->getAttributeLabel($attribute)])
            ->label(false);
    }
}