<?php

namespace Steady\Engine\Behaviors;

use Steady\Engine\SW;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class CacheFlushBehavior extends Behavior
{
    /**
     * @var string
     */
    public $key;

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        parent::attach($owner);

        if (!$this->key) {
            $this->key = constant(get_class($owner) . '::CACHE_KEY');
        }
    }

    /**
     * @inheritdoc
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'flush',
            ActiveRecord::EVENT_AFTER_UPDATE => 'flush',
            ActiveRecord::EVENT_AFTER_DELETE => 'flush',
        ];
    }

    /**
     * Flush cache
     */
    public function flush()
    {
        if ($this->key) {
            if (is_array($this->key)) {
                foreach ($this->key as $key) {
                    SW::$app->cache->delete($key);
                }
            } else {
                SW::$app->cache->delete($this->key);
            }
        } else {
            SW::$app->cache->flush();
        }
    }
}