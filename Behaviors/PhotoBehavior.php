<?php

namespace Steady\Engine\Behaviors;

use Steady\Engine\Base\Model;
use Steady\Engine\Base\Query;
use Steady\Engine\Models\PhotoModel;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property Model owner
 * @property PhotoModel[] photos
 */
class PhotoBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterDelete()
    {
        if ($this->photos) {
            foreach ($this->photos as $photo) {
                if ($photo instanceof PhotoModel) {
                    $photo->delete();
                }
            }
        }
    }

    /**
     * @return Query|ActiveQuery
     */
    public function getPhotos()
    {
        return $this->owner->hasMany(PhotoModel::class, ['owner_id' => $this->owner->primaryKey()[0]])
            ->where(['class_alias' => $this->owner->getAlias()]);
    }
}