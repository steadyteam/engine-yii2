<?php

namespace Steady\Engine\Behaviors;

use Steady\Engine\Base\Model;
use Steady\Engine\Components\RulesBehavior;
use Steady\Engine\Models\TagAssignModel;
use Steady\Engine\Models\TagModel;
use Steady\Engine\SW;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property Model owner
 * @property TagModel[] tags
 */
class TaggableBehavior extends RulesBehavior
{
    /**
     * @var array
     */
    private $_tags;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function rules(): array
    {
        return [
            ['tagsName', 'safe'],
        ];
    }

    /**
     * @throws \yii\db\Exception
     */
    public function afterSave()
    {
        /**
         * TODO: Возможно стоит сравнивать новые тэги с текущими, и удалять только отсутсвующие
         * Удаляет все тэги для записи, если она не новая.
         * После удаления текущих тэгов, добавляет новые тэги из аттрибута tagsName,
         * который высылаются из формы при сохранении модели
         */
        if (!$this->owner->isNewRecord) {
            $this->beforeDelete();
        }

        if ($this->_tags && count($this->_tags)) {
            $tagAssigns = [];
            $updatedTags = [];

            foreach ($this->_tags as $name) {
                if (!($tag = TagModel::findOne(['name' => $name]))) {
                    $tag = new TagModel(['name' => $name]);
                }
                $tag->frequency++;
                if ($tag->save()) {
                    $updatedTags[] = $tag;
                    $tagAssigns[] = [$tag->tag_id, $this->owner->primaryKey, $this->owner->getAlias()];
                }
            }

            if (count($tagAssigns)) {
                SW::$app->db->createCommand()
                    ->batchInsert(TagAssignModel::tableName(), ['tag_id', 'owner_id', 'class_alias'], $tagAssigns)
                    ->execute();
                $this->owner->populateRelation('tags', $updatedTags);
            }
        }
    }

    public function beforeDelete()
    {
        $pks = [];

        foreach ($this->owner->tags as $tag) {
            $pks[] = $tag->primaryKey;
        }

        if (count($pks)) {
            TagModel::updateAllCounters(['frequency' => -1], ['in', 'tag_id', $pks]);
        }
        TagModel::deleteAll(['frequency' => 0]);
        TagAssignModel::deleteAll(['owner_id' => $this->owner->primaryKey, 'class_alias' => $this->owner->getAlias()]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTags(): ActiveQuery
    {
        return $this->owner->hasMany(TagModel::class, ['tag_id' => 'tag_id'])
            ->via('tagAssigns');
    }

    /**
     * @return ActiveQuery
     */
    public function getTagAssigns(): ActiveQuery
    {
        return $this->owner->hasMany(TagAssignModel::class, ['owner_id' => $this->owner->primaryKey()[0]])
            ->where([TagAssignModel::tableName() . '.class_alias' => $this->owner->getAlias()]);
    }

    /**
     * @return string
     */
    public function getTagsName(): string
    {
        return implode(', ', $this->getTagsArray());
    }

    /**
     * @name string|array $values
     */
    public function setTagsName($values)
    {
        $this->_tags = $this->filterTagValues($values);
    }

    /**
     * @return array
     */
    public function getTagsArray(): array
    {
        if ($this->_tags === null) {
            $this->_tags = [];
            foreach ($this->owner->tags as $tag) {
                $this->_tags[] = $tag->name;
            }
        }
        return $this->_tags;
    }

    /**
     * Filters tags
     * @name string|array $values
     * @return array
     */
    private function filterTagValues($values): array
    {
        $valuesData = is_array($values) ? implode(',', $values) : $values;

        $replaced = preg_replace('/\s+/u', ' ', $valuesData);

        return array_unique(preg_split('/\s*,\s*/u', $replaced, -1, PREG_SPLIT_NO_EMPTY));
    }
}