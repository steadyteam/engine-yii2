<?php

namespace Steady\Engine\Behaviors;

use Steady\Engine\Base\Model;
use Steady\Engine\Models\SeoTextModel;
use Steady\Engine\SW;
use yii\base\Behavior;
use yii\console\Application;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property Model|SeoBehavior owner
 * @property SeoTextModel seoText
 * @property SeoTextModel seoTextRelation
 */
class SeoBehavior extends Behavior
{
    /**
     * @var SeoTextModel
     */
    private $_model;

    /**
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterInsert()
    {
        if (!SW::$app instanceof Application && $this->seoText->load(SW::$app->request->post())) {
            if (!$this->seoText->isEmpty()) {
                $this->seoText->save();
            }
        }
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterUpdate()
    {
        if (!SW::$app instanceof Application && $this->seoText->load(SW::$app->request->post())) {
            if (!$this->seoText->isEmpty()) {
                $this->seoText->save();
            } else {
                if ($this->seoText->primaryKey) {
                    $this->seoText->delete();
                }
            }
        }
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterDelete()
    {
        if (!$this->seoText->isNewRecord) {
            $this->seoText->delete();
        }
    }

    /**
     * @return ActiveQuery
     */
    public function getSeoTextRelation(): ActiveQuery
    {
        return $this->owner->hasOne(SeoTextModel::class, ['owner_id' => $this->owner->primaryKey()[0]])
            ->where([SeoTextModel::tableName() . '.class_alias' => $this->owner->getAlias()]);
    }

    /**
     * @return SeoTextModel
     */
    public function getSeoText(): SeoTextModel
    {
        if (!$this->_model) {
            $this->_model = $this->owner->seoTextRelation;
            if (!$this->_model) {
                $this->_model = new SeoTextModel([
                    'class_alias' => $this->owner->getAlias(),
                    'owner_id' => $this->owner->primaryKey,
                ]);
            }
        }

        return $this->_model;
    }
}