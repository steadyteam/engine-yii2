<?php

namespace Steady\Engine\Behaviors;

use Steady\Engine\Base\Model;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * @property Model owner
 */
class SortableModelBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'findMaxOrderNum',
        ];
    }

    public function findMaxOrderNum()
    {
        if (!$this->owner->order_num) {
            $maxOrderNum = (int)(new \yii\db\Query())
                ->select('MAX(`order_num`)')
                ->from($this->owner->tableName())
                ->scalar();
            $this->owner->order_num = ++$maxOrderNum;
        }
    }
}