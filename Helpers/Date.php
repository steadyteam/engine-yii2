<?php

namespace Steady\Engine\Helpers;

use Steady\Engine\SW;

class Date
{
    public static function getLocale()
    {
        return strtolower(substr(SW::$app->language, 0, 2));
    }
}