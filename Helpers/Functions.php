<?php

namespace Steady\Engine\Helpers;

class Functions
{
    public static function getSubclassesOf($parent)
    {
        $result = [];
        foreach (get_declared_classes() as $class) {
            if (is_subclass_of($class, $parent))
                $result[] = $class;
        }
        return $result;
    }
}