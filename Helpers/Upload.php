<?php

namespace Steady\Engine\Helpers;

use Steady\Engine\SW;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\web\HttpException;
use yii\web\UploadedFile;

class Upload
{
    /**
     * @name UploadedFile $fileInstance
     * @name string $dir
     * @name bool $namePostfix
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public static function file(UploadedFile $fileInstance, $dir = '', $namePostfix = true)
    {
        $fileName = Upload::getUploadPath($dir) . DIRECTORY_SEPARATOR . Upload::getFileName($fileInstance, $namePostfix);

        if (!$fileInstance->saveAs($fileName)) {
            throw new HttpException(500, "Cannot upload file $fileName. Please check write permissions.");
        }
        return Upload::getLink($fileName);
    }

    /**
     * @name $dir
     * @return string
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public static function getUploadPath($dir)
    {
        $uploadPath = SW::getAlias('@uploads') . ($dir ? DIRECTORY_SEPARATOR . $dir : '');
        if (!FileHelper::createDirectory($uploadPath)) {
            throw new HttpException(500, "Cannot create $uploadPath. Please check write permissions.");
        }

        return $uploadPath;
    }

    /**
     * @name $fileName
     * @return mixed
     */
    public static function getLink($fileName)
    {
        return str_replace('\\', '/', str_replace(SW::getAlias('@uploads'), STD_URL_UPLOADS, $fileName));
    }

    /**
     * @name $fileInstance
     * @name bool $namePostfix
     * @return string
     */
    public static function getFileName($fileInstance, $namePostfix = true)
    {
        $baseName = str_ireplace('.' . $fileInstance->extension, '', $fileInstance->name);
        $fileName = StringHelper::truncate(Inflector::slug($baseName), 32, '');
        if ($namePostfix || !$fileName) {
            $fileName .= ($fileName ? '-' : '') . substr(uniqid(md5(rand()), true), 0, 10);
        }
        $fileName .= '.' . $fileInstance->extension;

        return $fileName;
    }
}