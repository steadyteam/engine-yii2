<?php

namespace Steady\Engine\Helpers;

use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use Steady\Modules\Email\Models\EmailModel;

class Mail
{
    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $fromName;

    /**
     * @var string
     */
    private $to;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $type = "text/html";

    /**
     * @var string
     */
    private $charset = "utf-8";

    /**
     * @var bool
     */
    private $notify = false;

    public function __construct()
    {
    }

    /**
     * @return Mail
     */
    public static function newInstance()
    {
        return new self();
    }

    /**
     * @name $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @name $fromName
     * @return $this
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * @name $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @name $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @name $message
     * @return $this
     */
    public function setTextBody($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @name $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @name $notify
     * @return $this
     */
    public function setNotify($notify)
    {
        $this->notify = $notify;

        return $this;
    }

    /**
     * @name $charset
     * @return $this
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;

        return $this;
    }

    /**
     * @return bool
     */
    public function send()
    {
        $from = "=?utf-8?B?" . base64_encode($this->fromName) . "?= <$this->from>";

        $headers = "From: $from\r\nReply-To: $from\r\nContent-type: $this->type; charset=$this->charset\r\n";
        // Добавляем запрос подтверждения получения письма, если требуется
        if ($this->notify) $headers .= "Disposition-Notification-To: $this->from\r\n";

        $subject = "=?utf-8?B?" . base64_encode($this->subject) . "?=";

        $result = mail($this->to, $subject, $this->message, $headers);

        // TODO: Временное решение, сделать без зависимостей
        if (SW::$app->getModule('admin')->getModule('email')) {
            /*$model = new EmailModel();
            $model->from = $this->from;
            $model->from_name = $this->fromName;
            $model->to = $this->to;
            $model->subject = $this->subject;
            $model->body = $this->message;
            $model->result = $result;
            $model->parent_id = 1;
            $model->save();*/
        }

        return $result;
    }

    /**
     * @name $toEmail
     * @name $subject
     * @name $layout
     * @name array $data
     * @name array $options
     * @return bool
     */
    public static function sendMailer($toEmail, $subject, $layout, $data = [], $options = [])
    {
        if (!filter_var($toEmail, FILTER_VALIDATE_EMAIL) || !$subject || !$layout) {
            return false;
        }
        $data['subject'] = trim($subject);

        $message = SW::$app->mailer->compose($layout, $data)
            ->setTo($toEmail)
            ->setSubject($data['subject']);

        if (filter_var(SettingModel::get('robot_email'), FILTER_VALIDATE_EMAIL)) {
            $message->setFrom(SettingModel::get('robot_email'));
        }

        if (!empty($options['replyTo']) && filter_var($options['replyTo'], FILTER_VALIDATE_EMAIL)) {
            $message->setReplyTo($options['replyTo']);
        }

        return $message->send();
    }
}