<?php

namespace Steady\Engine\Helpers;

use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;
use Steady\Engine\SW;
use yii\helpers\FileHelper;
use yii\web\HttpException;
use yii\web\UploadedFile;

class Image
{
    /**
     * @name UploadedFile $fileInstance
     * @name string $dir
     * @name null $resizeWidth
     * @name null $resizeHeight
     * @name bool $resizeCrop
     * @return mixed
     * @throws HttpException
     * @throws \ImagickException
     * @throws \yii\base\Exception
     */
    public static function upload(UploadedFile $fileInstance, $dir = '', $resizeWidth = null, $resizeHeight = null,
                                  $resizeCrop = false)
    {
        $fileName = Upload::getUploadPath($dir) . DIRECTORY_SEPARATOR . Upload::getFileName($fileInstance);

        $uploaded = $resizeWidth
            ? self::copyResizedImage($fileInstance->tempName, $fileName, $resizeWidth, $resizeHeight, $resizeCrop)
            : $fileInstance->saveAs($fileName);

        if (!$uploaded) {
            throw new HttpException(500, 'Cannot upload file "' . $fileName . '". Please check write permissions.');
        }

        return Upload::getLink($fileName);
    }

    /**
     * @name $inputFile
     * @name $outputFile
     * @name $width
     * @name null $height
     * @name bool $crop
     * @return bool
     * @throws HttpException
     * @throws \ImagickException
     */
    public static function copyResizedImage($inputFile, $outputFile, $width, $height = null, $crop = true)
    {
        if (extension_loaded('gd')) {
            $image = new GD($inputFile);

            if ($height) {
                if ($width && $crop) {
                    $image->cropThumbnail($width, $height);
                } else {
                    $image->resize($width, $height);
                }
            } else {
                $image->resize($width);
            }
            return $image->save($outputFile);
        } else if (extension_loaded('imagick')) {
            $image = new \Imagick($inputFile);

            if ($height && !$crop) {
                $image->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1, true);
            } else {
                $image->resizeImage($width, null, \Imagick::FILTER_LANCZOS, 1);
            }

            if ($height && $crop) {
                $image->cropThumbnailImage($width, $height);
            }

            return $image->writeImage($outputFile);
        } else {
            throw new HttpException(500, 'Please install GD or Imagick extension');
        }
    }

    /**
     * @param $filename
     * @param null $width
     * @param null $height
     * @param bool $crop
     * @param bool $useNoImage
     * @return string
     */
    public static function thumb($filename, $width = null, $height = null, $crop = true, $useNoImage = true)
    {
        $filePath = $filename ? SW::getAlias('@root') . $filename : null;

        if ($useNoImage && (!file_exists($filePath) || !is_file($filePath))) {
            $filename = 'no_image.png';
            $filePath = SW::getAlias("@static/img/base/$filename");
        }

        if (file_exists($filePath)) {
            $info = pathinfo($filePath);
            $hash = md5(filemtime($filePath) . (int)$width . (int)$height . (int)$crop);
            $thumbName = $info['filename'] . '-' . $hash . '.' . ($info['extension'] ?? '');
            $thumbFile = SW::getAlias("@uploads/thumbs/$thumbName");
            $thumbWebFile = STD_URL_UPLOADS . '/thumbs/' . $thumbName;
            try {
                if (file_exists($thumbFile)) {
                    return $thumbWebFile;
                } else if (FileHelper::createDirectory(dirname($thumbFile), 0777) &&
                    self::copyResizedImage($filePath, $thumbFile, $width, $height, $crop)) {
                    return $thumbWebFile;
                }
            } catch (\Exception $e) {
                return $filename;
            }
        }
        return null;
    }

    public static function watermark($imageFilename, $watermarkFilename)
    {
        try {
            $imageFilename = SW::getAlias("@root$imageFilename");
            $watermarkFilename = SW::getAlias($watermarkFilename);

            $imagine = new Imagine();

            $image = $imagine->open($imageFilename);
            $watermark = $imagine->open($watermarkFilename);
            $size = $image->getSize();
            $wSize = $watermark->getSize();

            if ($size->getWidth() < $wSize->getWidth() || $size->getHeight() < $wSize->getHeight()) {
                $watermark->resize(new Box($wSize->getWidth() / 2, $wSize->getHeight() / 2));
                $wSize = $watermark->getSize();
            }

            $bottomRight = new Point(
                ($size->getWidth() - $wSize->getWidth()) / 2,
                ($size->getHeight() - $wSize->getHeight()) / 2
            );

            $image->paste($watermark, $bottomRight);

            $result = $image->save();
        } catch (\Exception $e) {
            return false;
        }

        return $result;
    }
}