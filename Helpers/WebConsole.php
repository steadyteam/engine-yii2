<?php

namespace Steady\Engine\Helpers;

use Steady\Engine\SW;
use yii\console\Application as ConsoleApplication;
use yii\helpers\FileHelper;
use yii\web\ServerErrorHttpException;

class WebConsole
{
    /**
     * @var string
     */
    public $logFileName;
    /**
     * @var bool|resource
     */
    public $logFileHandler;
    /**
     * @var ConsoleApplication
     */
    private $_console;
    /**
     * @var string
     */
    private $_configFileName;

    /**
     * @name string $configFileName
     */
    public function __construct(string $configFileName)
    {
        $this->_configFileName = $configFileName;
    }

    /**
     * @return ConsoleApplication
     * @throws \yii\base\Exception
     * @throws ServerErrorHttpException
     */
    private function console(): ConsoleApplication
    {
        if (!$this->_console) {
            $logsPath = SW::getAlias('@runtime/logs');
            if (!FileHelper::createDirectory($logsPath, 0777)) {
                throw new ServerErrorHttpException('Cannot create `' . $logsPath . '`. Please check write permissions.');
            }

            $this->logFileName = $logsPath . DIRECTORY_SEPARATOR . 'console.log';
            $this->logFileHandler = fopen($this->logFileName, 'w+');

            defined('STDIN') or define('STDIN', $this->logFileHandler);
            defined('STDOUT') or define('STDOUT', $this->logFileHandler);

            $oldApp = SW::$app;

            if (!file_exists($this->_configFileName)) {
                throw new ServerErrorHttpException("Cannot find `$this->_configFileName`. Please create console config.");
            }

            $consoleConfig = require($this->_configFileName);

            if (!is_array($consoleConfig)) {
                throw new ServerErrorHttpException("Config is not array! Check `$this->_configFileName` file");
            }

            $this->_console = new ConsoleApplication($consoleConfig);

            SW::$app = $oldApp;
        } else {
            ftruncate($this->logFileHandler, 0);
        }

        return $this->_console;
    }

    /**
     * @name string $command
     * @name array $params
     * @return string
     * @throws ServerErrorHttpException
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     */
    public function execute(string $command, array $params = []): string
    {
        return $this->command($command, $params);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     * @throws ServerErrorHttpException
     */
    public function migrate(): string
    {
        return $this->migrateCommand('migrate');
    }

    /**
     * @return string
     * @throws ServerErrorHttpException
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     */
    public function migrateDown(): string
    {
        return $this->migrateCommand('migrate/down');
    }

    /**
     * @name string $command
     * @return string
     * @throws ServerErrorHttpException
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     */
    private function migrateCommand(string $command): string
    {
        return $this->command($command, [
            'migrationPath' => STD_PATH_INSTALL . '/Migrations/',
            'migrationTable' => 'migration',
            'interactive' => false,
        ]);
    }

    /**
     * @name string $command
     * @name array $params
     * @return string
     * @throws ServerErrorHttpException
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     */
    private function command(string $command, array $params = []): string
    {
        ob_start();

        $this->console()->runAction($command, $params);

        $result = file_get_contents($this->logFileName) . "\n" . ob_get_clean();

        SW::$app->cache->flush();

        return $result;
    }
}