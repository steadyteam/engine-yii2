<?php

namespace Steady\Engine\Helpers;

use Steady\Engine\SW;

class CacheHelper
{
    /**
     * @name string $key
     * @name int $duration
     * @name callable $callable
     * @return mixed
     */
    public static function cache(string $key, int $duration, callable $callable)
    {
        $cache = SW::$app->cache;
        if ($cache->exists($key)) {
            $data = $cache->get($key);
        } else {
            $data = $callable();

            if ($data) {
                $cache->set($key, $data, $duration);
            }
        }
        return $data;
    }
}