<?php

namespace Steady\Engine\Exceptions;

class NotImplementedException extends \LogicException
{
    public function getName()
    {
        return 'NotImplementedException';
    }
}