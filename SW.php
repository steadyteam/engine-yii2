<?php

namespace Steady\Engine;

class SW extends \Yii
{
    /**
     * @inheritdoc
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        // TODO: Временно, для работы установки
        if (strstr($category, 'admin') && !SW::$app->getModule('admin')) return $message;

        if (!isset(SW::$app->i18n->translations[$category])) return $message;

        return parent::t($category, $message, $params, $language);
    }

    /**
     * @return bool
     */
    public static function isDbConnected(): bool
    {
        try {
            SW::$app->db->open();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @name string $moduleName
     * @return array
     */
    public static function getModuleParams($moduleName)
    {
        return SW::$app->getModule('admin')->getModule($moduleName)->params ?? [];
    }

    /**
     * @name $moduleName
     */
    public static function getNamesByModule($moduleName)
    {

    }
}
